// JavaScript Document
/* \file Wish_Recherche.js
 * \brief Fichier de definition des methodes de la classe Wish_Recherche
 * \author Tanguy Lambert
 * \version 1.0
 * \date 19 aout 2010
 *
 * Ce fichier contient les methodes de gestion des recherches
 */
/**
 * \fn Wish_Recherche
 * \brief Constructeur
 *
 * \param language : langue utilisee pour les messages
 * \param id_form_resultats identifiant du formulaire des résultats
 * \param name_check_item nom de la liste des checkbox
 * \param id_form_search identifiant du formulaire de recherche
 */
Wish_Recherche = function(varName, language, id_form_resultats, name_check_item, id_form_search) {
    this.varName = varName;
    this.urlComp = [];
    this.tabCategory = [];
    this.forCategory = -1;
    if(language) this.language = language; else this.language = 'fr';
    if(id_form_search) this.id_form_search = id_form_search; else this.id_form_search = 'boondrecherche';
    if(id_form_resultats) this.id_form_resultats = id_form_resultats; else this.id_form_resultats = 'mesresultats';
    if(name_check_item) this.name_check_item = name_check_item; else this.name_check_item = 'chk_result[]';
}

Wish_Recherche.prototype.manageImgButtons = function(type)
{
	var bimg = document.getElementById('savecookie');
	bimg.onclick = function() {
		switch(document.getElementById('savesearch').value){
			case '1':bimg.src = 'images/restore.png';document.getElementById('savesearch').value = '2';break;
			case '2':bimg.src = 'images/notstore.png';document.getElementById('savesearch').value='0';break;
			default:bimg.src = 'images/store.png';document.getElementById('savesearch').value='1';break;
		}
	};
	switch(this.language) {
		case 'en':tipsyCookie = '<img src="images/notstore.png" width="20px" height="20px" alt="Cookie" /> Do not store this search<br /><img src="images/store.png" width="20px" height="20px" alt="Cookie" /> Store this search<br /><img src="images/restore.png" width="20px" height="20px" alt="Cookie" /> Reset searching filters<br /><br /><span class="legende" style="font-size:9px">Please click on <img src="images/molette_on.png" width="15px" height="15px" alt="Cookie" /> (Storing search) for modify default behavior</span>';break;
		default:tipsyCookie = '<img src="images/notstore.png" width="20px" height="20px" alt="Cookie" /> Ne pas mémoriser cette recherche<br /><img src="images/store.png" width="20px" height="20px" alt="Cookie" /> Mémoriser cette recherche<br /><img src="images/restore.png" width="20px" height="20px" alt="Cookie" /> Réinitialiser les filtres de recherche<br /><br /><span class="legende" style="font-size:9px">Veuillez cliquer sur <img src="images/molette_on.png" width="15px" height="15px" alt="Cookie" /> (Mémorisation des recherches) pour modifier le comportement par défaut</span>';break;
	}
	$('#savecookie').tipsy({gravity: 'ne', html: true, fade: true, title:function(){return tipsyCookie;}});
	if(type) {
		var bimg2 = document.getElementById('typesearchimg');
		bimg2.onclick = function() {
			switch(document.getElementById('cv').value){
				case '1':bimg2.src = 'images/dt.png';document.getElementById('cv').value = '2';break;
				case '2':bimg2.src = 'images/database.png';document.getElementById('cv').value='0';break;
				default:bimg2.src = 'images/cv.png';document.getElementById('cv').value='1';break;
			}
		};
	}
}

/**
 * \fn search_order_colonne(name_colonne)
 * \brief Soumet le formulaire de recherche en indiquant la colonne à ordonner et le type d'ordonnancement
 * \param name_colonne nom de la colonne à ordonner
 * \param array_var_mc tableau des variables Wish_Multicriteres
 */
Wish_Recherche.prototype.search_order_colonne = function(name_colonne, array_var_mc)
{
    if( document.getElementById('oc').value == name_colonne ) {
        switch(document.getElementById('ot').value) {
            case 'asc':document.getElementById('ot').value = 'desc';break;
            case 'desc':document.getElementById('ot').value = 'asc';break;}
    } else {
        document.getElementById('oc').value = name_colonne;
        document.getElementById('ot').value = 'asc';
    }
    if(array_var_mc) {jQuery.each(array_var_mc, function(i, varmc) {varmc.getSelection();});}
    document.getElementById(this.id_form_search).submit();
}

/**
 * \fn updateFormAction
 * \brief Exécute l'action spécifique à un bouton sur une liste de checkbox
 * \param type page web requise
 * \param action action sollicitée
 * \param comp complément de requete passé en paramètres
 */
Wish_Recherche.prototype.updateFormAction = function(type, action, comp, reference, force_etat, method, confirmMsg, urlComp) {
	this.urlComp = [];
    var etat = false;
    if(force_etat!=undefined) etat = force_etat;
    liste = document.getElementsByName(this.name_check_item);
    for(var i=0; i < liste.length; i++) {if( liste[i].checked == true ) {etat = true;break;}}
    if(etat) {
        if(comp==undefined) comp = '';
        if(reference==undefined) reference = '';
        if(method==undefined) method = '';
        if(confirmMsg==undefined) confirmMsg = '';
        if(urlComp!=undefined) this.urlComp.push(urlComp);
        fcnt = this.varName+'.actionItems(\''+type+'/'+action+comp+'\',\''+method+'\')';
		imgWarn = '<img alt="warning" height="25px" width="25px" src="images/warning.png" style="margin-bottom:-5px;margin-right:5px;" />';
        switch(action) {
            case 'supprimer':case 'supprimer-objectifs':case 'supprimer-avantages':case 'supprimer-inactivites':
				var defaultConfirm = false;
                switch(type) {
                    case 'tableau-de-bord/liste-flags' :
                        switch(this.language) {
                            default:confirm(imgWarn+'Pour éviter la perte de données, tout flag ayant l\'un des objets suivants attaché ne sera pas supprimé :<br /><br /><ul><li><p style="text-align:left">Ressources</p></li><li><p style="text-align:left">Candidats</p></li><li><p style="text-align:left">Sociétés CRM</p></li><li><p style="text-align:left">Besoins</p></li><li><p style="text-align:left">Projets</p></li><li><p style="text-align:left">Commandes</p></li><li><p style="text-align:left">Produits</p></li><li><p style="text-align:left">Achats</p></li><li><p style="text-align:left">Actions</p></li><li><p style="text-align:left">Positionnements</p></li><li><p style="text-align:left">Contacts CRM</p></li><li><p style="text-align:left">Factures</p></li></ul><br/><br/>Désirez-vous procéder à la suppression ?', fcnt, '', this.language);break;
                            case 'en':confirm(imgWarn+'In order to avoid data loss, every flag for which one of the following object is attached won\'t be deleted :<br/><br/><ul><li><p style="text-align:left">Resources</p></li><li><p style="text-align:left">Candidates</p></li><li><p style="text-align:left">CRM Companies</p></li><li><p style="text-align:left">Opportunities</p></li><li><p style="text-align:left">Projects</p></li><li><p style="text-align:left">Orders</p></li><li><p style="text-align:left">Products</p></li><li><p style="text-align:left">Purchases</p></li><li><p style="text-align:left">Actions</p></li><li><p style="text-align:left">Positionings</p></li><li><p style="text-align:left">CRM Contacts</p></li><li><p style="text-align:left">Invoices</p></li></ul><br/><br/>Do you want to proceed with the deletion ?', fcnt, '', this.language);break;
                        }
                        break;
                    case 'tableau-de-bord/liste-ressources':
                        switch(this.language) {
							default:confirm(imgWarn+'Pour éviter la perte de données, toute ressource ayant l\'un des objets suivants attaché ne sera pas supprimé :<br /><br /><ul><li><p style="text-align:left">Positionnements</p></li><li><p style="text-align:left">Prestations</p></li><li><p style="text-align:left">Feuilles des temps</p></li><li><p style="text-align:left">Notes de frais</p></li><li><p style="text-align:left">Demandes d\'absences</p></li><li><p style="text-align:left">Validations</p></li><li><p style="text-align:left">Quotas d\'absences</p></li><li><p style="text-align:left">Avantages</p></li></ul><br/><br/>Désirez-vous procéder à la suppression ?', fcnt, '', this.language);break;
                            case 'en':confirm(imgWarn+'In order to avoid data loss, every resource for which one of the following object is attached won\'t be deleted :<br/><br/><ul><li><p style="text-align:left">Positionings</p></li><li><p style="text-align:left">Deliveries</p></li><li><p style="text-align:left">Timesheets</p></li><li><p style="text-align:left">Expenses</p></li><li><p style="text-align:left">Leaves requests</p></li><li><p style="text-align:left">Validations</p></li><li><p style="text-align:left"></p></li><li><p style="text-align:left">Triggers accounts</p></li><li><p style="text-align:left">Advantages</p></li></ul><br/><br/>Do you want to proceed with the deletion ?', fcnt, '', this.language);break;
                        }
                        break;
					case 'tableau-de-bord/liste-candidats':
                        switch(this.language) {
							default:confirm(imgWarn+'Pour éviter la perte de données, tout candidat ayant l\'un des objets suivants attaché ne sera pas supprimé :<br /><br /><ul><li><p style="text-align:left">Positionnements</p></li><li><p style="text-align:left">Projets</p></li></ul><br/><br/>Désirez-vous procéder à la suppression ?', fcnt, '', this.language);break;
                            case 'en':confirm(imgWarn+'In order to avoid data loss, every candidate for which one of the following object is attached won\'t be deleted :<br/><br/><ul><li><p style="text-align:left">Positionings</p></li><li><p style="text-align:left">Projects</p></li></ul><br/><br/>Do you want to proceed with the deletion ?', fcnt, '', this.language);break;
                        }
                        break;
                    case 'tableau-de-bord/liste-projets' :
						if(comp.substr(0,5) == '?td=1') {//Ce message apparaît uniquement pour les projets
							switch(this.language) {
								default:confirm(imgWarn+'Pour éviter la perte de données, toute prestation ayant l\'un des objets suivants attaché ne sera pas supprimé :<br /><br /><ul><li><p style="text-align:left">Temps</p></li><li><p style="text-align:left">Frais</p></li><li><p style="text-align:left">Achats</p></li><li><p style="text-align:left">Commandes</p></li><li><p style="text-align:left">Prestations cédées</p></li></ul><br /><span class="legende">Dans le cas d\'une prestation de recrutement, cela supprimera également le projet attaché !</span><br/><br/><br/>Désirez-vous procéder à la suppression ?', fcnt, '', this.language);break;
								case 'en':confirm(imgWarn+'In order to avoid data loss, every delivery for which one of the following object is attached won\'t be deleted :<br/><br/><ul><li><p style="text-align:left">Times</p></li><li><p style="text-align:left">Expenses</p></li><li><p style="text-align:left">Purchases</p></li><li><p style="text-align:left">Orders</p></li><li><p style="text-align:left">Transfered deliveries</p></li></ul><br /><span class="legende">In case of recruitment\'s delivery, it will also delete attached project !</span><br/><br/><br/>Do you want to proceed with the deletion ?', fcnt, '', this.language);break;
							}
						} else {
							switch(this.language) {
								default:confirm(imgWarn+'Pour éviter la perte de données, tout projet ayant l\'un des objets suivants attaché ne sera pas supprimé :<br /><br /><ul><li><p style="text-align:left">Prestations</p></li><li><p style="text-align:left">Commandes</p></li><li><p style="text-align:left">Achats</p></li></ul><br/><br/>Désirez-vous procéder à la suppression ?', fcnt, '', this.language);break;
								case 'en':confirm(imgWarn+'In order to avoid data loss, every project for which one of the following object is attached won\'t be deleted :<br/><br/><ul><li><p style="text-align:left">Deliveries</p></li><li><p style="text-align:left">Orders</p></li><li><p style="text-align:left">Purchases</p></li></ul><br/><br/>Do you want to proceed with the deletion ?', fcnt, '', this.language);break;
							}
						}
                        break;
                    case 'tableau-de-bord/liste-crm':
						if(comp.substr(0,5) == '?tf=1') {//Ce message apparaît uniquement pour les contacts CRM
							switch(this.language) {
								default:confirm(imgWarn+'Pour éviter la perte de données, tout contact CRM ayant l\'un des objets suivants attaché ne sera pas supprimé :<br /><br /><ul><li><p style="text-align:left">Devis</p></li><li><p style="text-align:left">Besoins</p></li><li><p style="text-align:left">Projets</p></li><li><p style="text-align:left">Ressources</p></li><li><p style="text-align:left">Achats</p></li></ul><br/><br/>Désirez-vous procéder à la suppression ?', fcnt, '', this.language);break;
								case 'en':confirm(imgWarn+'In order to avoid data loss, every CRM contact for which one of the following object is attached won\'t be deleted :<br/><br/><ul><li><p style="text-align:left">Quotations</p></li><li><p style="text-align:left">Opportunities</p></li><li><p style="text-align:left">Projects</p></li><li><p style="text-align:left">Resources</p></li><li><p style="text-align:left">Purchases</p></li></ul><br/><br/>Do you want to proceed with the deletion ?', fcnt, '', this.language);break;
							}
						} else {
							switch(this.language) {
								default:confirm(imgWarn+'Pour éviter la perte de données, toute société CRM ayant l\'un des objets suivants attaché ne sera pas supprimé :<br /><br /><ul><li><p style="text-align:left">Contacts CRM</p></li><li><p style="text-align:left">Commandes</p></li><li><p style="text-align:left">Agences</p></li><li><p style="text-align:left">Besoins</p></li><li><p style="text-align:left">Projets</p></li><li><p style="text-align:left">Resssources</p></li><li><p style="text-align:left">Achats</p></li></ul><br/><br/>Désirez-vous procéder à la suppression ?', fcnt, '', this.language);break;
								case 'en':confirm(imgWarn+'In order to avoid data loss, every CRM company for which one of the following object is attached won\'t be deleted :<br/><br/><ul><li><p style="text-align:left">CRM Contacts</p></li><li><p style="text-align:left">Orders</p></li><li><p style="text-align:left">Agencies</p></li><li><p style="text-align:left">Opportunities</p></li><li><p style="text-align:left">Projects</p></li><li><p style="text-align:left">Resources</p></li><li><p style="text-align:left">Purchases</p></li></ul><br/><br/>Do you want to proceed with the deletion ?', fcnt, '', this.language);break;
							}
						}
                        break;
                    case 'tableau-de-bord/liste-temps-frais-absences' :
                        this.urlComp.push('deltype');
                        switch(this.language) {
                            default:confirm('Vous souhaitez supprimer <select id="deltype" class="normalselection"><option value="0">les documents</option><option value="1" selected="selected">les validateurs</option></select> sélectionnés.<br/>'+imgWarn+'Les documents ayant été clôturées ne pourront pas être supprimées.<br/><br/>Désirez-vous procéder à la suppression ?', fcnt, '', this.language);break;
                            case 'en':confirm('You want to delete selected <select id="deltype" class="normalselection"><option value="0">documents</option><option value="1" selected="selected">validators</option></select><br/>'+imgWarn+'Documents which have been closed won\'t be deleted.<br/><br/>Do you want to proceed with the deletion ?', fcnt, '', this.language);break;
                        }
                        break;
                    case 'tableau-de-bord/liste-produits' :
                        switch(this.language) {
							default:confirm(imgWarn+'Pour éviter la perte de données, tout produit ayant l\'un des objets suivants attaché ne sera pas supprimé :<br /><br /><ul><li><p style="text-align:left">Positionnements</p></li><li><p style="text-align:left">Prestations</p></li></ul><br/><br/>Désirez-vous procéder à la suppression ?', fcnt, '', this.language);break;
                            case 'en':confirm(imgWarn+'In order to avoid data loss, every product for which one of the following object is attached won\'t be deleted :<br/><br/><ul><li><p style="text-align:left">Positionings</p></li><li><p style="text-align:left">Deliveries</p></li></ul><br/><br/>Do you want to proceed with the deletion ?', fcnt, '', this.language);break;
                        }
                        break;
                    case 'tableau-de-bord/liste-commandes-factures':
                        if(comp.substr(0,5) == '?td=1' || comp.substr(0,5) == '?td=2' || comp.substr(0,5) == '?td=3') {//Ce message apparaît uniquement pour les commande
                            switch(this.language) {
								default:confirm(imgWarn+'Pour éviter la perte de données, toute commande ayant l\'un des objets suivants attaché ne sera pas supprimé :<br /><br /><ul><li><p style="text-align:left">Factures</p></li></ul><br/><br/>Désirez-vous procéder à la suppression ?', fcnt, '', this.language);break;
								case 'en':confirm(imgWarn+'In order to avoid data loss, every order for which one of the following object is attached won\'t be deleted :<br/><br/><ul><li><p style="text-align:left">Invoices</p></li></ul><br/><br/>Do you want to proceed with the deletion ?', fcnt, '', this.language);break;
                            }
                        } else if(comp.substr(0,5) == '?td=0') {//Ce message apparaît uniquement pour les factures
                            switch(this.language) {
								default:confirm(imgWarn+'Les factures ayant été clôturées ne pouront pas être supprimées.<br/><br/>Désirez-vous procéder à la suppression ?', fcnt, '', this.language);break;
								case 'en':confirm(imgWarn+'Bills which have been closed won\'t be deleted.<br/><br/>Do you want to proceed with the deletion ?', fcnt, '', this.language);break;
                            }
                        } else defaultConfirm = true;
						break;
					case 'tableau-de-bord/liste-achats':
                        if(comp.substr(0,6) == '?cat=0') {//Ce message apparaît uniquement pour les achats
                            switch(this.language) {
								default:confirm(imgWarn+'Pour éviter la perte de données, tout achat ayant l\'un des objets suivants attaché ne sera pas supprimé :<br /><br /><ul><li><p style="text-align:left">Commandes</p></li></ul><br/><br/>Désirez-vous procéder à la suppression ?', fcnt, '', this.language);break;
								case 'en':confirm(imgWarn+'In order to avoid data loss, every purchase for which one of the following object is attached won\'t be deleted :<br/><br/><ul><li><p style="text-align:left">Orders</p></li></ul><br/><br/>Do you want to proceed with the deletion ?', fcnt, '', this.language);break;
                            }
                        } else defaultConfirm = true;
						break;
                    default:defaultConfirm = true;break;
                }
				if(defaultConfirm) {
					switch(this.language) {
						default:confirm('Désirez-vous supprimer les items sélectionnés ?', fcnt, '', this.language);break;
						case 'en':confirm('Do you want to delete selected items ?', fcnt, '', this.language);break;
					}
				}
                break;
            case 'extraire':
                selectTxt = '';
                jQuery.each(reference, function(i, val) {selectTxt += '<option value="'+i+'">'+val+'</option>';});
                if(selectTxt != '') {
                    this.urlComp.push('ext_type');
                    switch(this.language) {
                        default:selectTxt = 'Veuillez sélectionner le type d\'extraction :<br/><br/><select id="ext_type" class="normalselection">'+selectTxt+'</select><br /><br />';break;
                        case 'en':selectTxt = 'Please select the type of extraction :<br/><br/><select id="ext_type" class="normalselection">'+selectTxt+'</select><br /><br />';break;
                    }
                }
                this.urlComp.push('ext_encoding');
                switch(this.language) {
                    default:confirm(selectTxt+'Veuillez sélectionner le format d\'encodage du fichier d\'extraction :<br/><br/><select id="ext_encoding" class="normalselection"><option value="ISO-8859-1" selected>ISO-8859-1</option><option value="WINDOWS-1250">WINDOWS-1250</option><option value="UTF-8">UTF-8</option><option value="MAC OS ROMAN">MAC OS ROMAN</option></select><br /><br />Désirez-vous procéder à l\'extraction ?', fcnt, '', this.language);break;
                    case 'en':confirm(selectTxt+'Please select the file extraction encoding format :<br/><br/><select id="ext_encoding" class="normalselection"><option value="ISO-8859-1" selected>ISO-8859-1</option><option value="WINDOWS-1250">WINDOWS-1250</option><option value="UTF-8">UTF-8</option><option value="MAC OS ROMAN">MAC OS ROMAN</option></select><br /><br />Do you want to proceed with the extraction ?', fcnt, '', this.language);break;
                }
                break;
            case 'exporter':
                selectTxt = '';
                jQuery.each(reference, function(i, val) {selectTxt += '<option value="'+i+'">'+val+'</option>';});
                if(selectTxt != '') {
                    this.urlComp.push('exp_type');
                    switch(this.language) {
                        default:selectTxt = 'Veuillez sélectionner le type d\'export :<br/><br/><select id="exp_type" class="longselection">'+selectTxt+'</select><br /><br />';break;
                        case 'en':selectTxt = 'Please select the type of export :<br/><br/><select id="exp_type" class="longselection">'+selectTxt+'</select><br /><br />';break;
                    }
                }
                switch(this.language) {
                    default:confirm(selectTxt+'Désirez-vous procéder à l\'exportation ?', fcnt, '', this.language);break;
                    case 'en':confirm(selectTxt+'Do you want to launch the export ?', fcnt, '', this.language);break;
                }
                break;
            case 'creer-prolongation':
                switch(this.language) {
                    default:confirm('Désirez-vous créer des prolongations pour les prestations sélectionnées ?', fcnt, '', this.language);break;
                    case 'en':confirm('Do you want to create extensions for selected deliveries ?', fcnt, '', this.language);break;
                }
                break;
            case 'modifier':
                if(type == 'tableau-de-bord/liste-crm' && comp == '?tf=0&field=etat') {
                    this.urlComp.push('allcontacts');
                    switch(this.language) {
                        default:moreTxtAffect = '<span style="font-size:10px"><input type="checkbox" value="" name="allcontacts" id="allcontacts" onchange="this.value=this.checked" />Cochez la case pour modifier également tous les contacts de cette société</span><br/><br/>';break;
                        case 'en':moreTxtAffect = '<span style="font-size:10px"><input type="checkbox" value="" name="allcontacts" id="allcontacts" onchange="this.value=this.checked" />Check the box to update all contacts of this company</span><br/><br/>';break;
                    }
                } else if(type == 'tableau-de-bord/liste-commandes-factures' && comp == '?td=0' && document.getElementById('aff_selection').value == '3') {
                	this.urlComp.push('wspayment');
                	var calHTML = '<td><div><input class="calendrier" name="wspayment" id="wspayment" style="cursor:text" value="'+reference+'" onclick="wscaldate.ds_sh(true);" /><table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclassdate" style="display:none;"><tr><td id="ds_calclassdate" style="margin:0px;padding:0px"></td></tr></table></div></td>';
                	switch(this.language) {
                        default:moreTxtAffect = '<table><tr><td><span style="font-size:10px">Règlement reçu le </span></td>'+calHTML+'</tr></table><br/><br/>';break;
                        case 'en':moreTxtAffect = '<table><tr><td><span style="font-size:10px">Payment received on </span></td>'+calHTML+'</tr></table><br/><br/>';break;
                    }
                } else if(type == 'tableau-de-bord/liste-achats' && comp == '?cat=1' && document.getElementById('aff_selection').value == '2') {
                	this.urlComp.push('wspayment');
                	var calHTML = '<td><div><input class="calendrier" name="wspayment" id="wspayment" style="cursor:text" value="'+reference+'" onclick="wscaldate.ds_sh(true);" /><table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclassdate" style="display:none;"><tr><td id="ds_calclassdate" style="margin:0px;padding:0px"></td></tr></table></div></td>';
                	switch(this.language) {
                        default:moreTxtAffect = '<table><tr><td><span style="font-size:10px">Paiement effectué le </span></td>'+calHTML+'</tr></table><br/><br/>';break;
                        case 'en':moreTxtAffect = '<table><tr><td><span style="font-size:10px">Payment performed </span></td>'+calHTML+'</tr></table><br/><br/>';break;
                    }
                } else moreTxtAffect = '';
                switch(this.language) {
                    default:confirm(moreTxtAffect+'Désirez-vous modifier les items sélectionnés ?', fcnt, '', this.language);break;
                    case 'en':confirm(moreTxtAffect+'Do you want to update selected items ?', fcnt, '', this.language);break;
                }
                wscaldate = new Wish_Calendar('wscaldate', 'wspayment', 'ds_calclassdate', 'ds_conclassdate', this.language);
                break;
            case 'affecter':
                if(type == 'tableau-de-bord/liste-crm' && comp == '?tf=0') {
                    this.urlComp.push('allcontacts');
                    switch(this.language) {
                        default:moreTxtAffect = '<span style="font-size:10px"><input type="checkbox" value="" name="allcontacts" id="allcontacts" onchange="this.value=this.checked" />Cochez la case pour réaffecter également tous les contacts de cette société</span><br/><br/>';break;
                        case 'en':moreTxtAffect = '<span style="font-size:10px"><input type="checkbox" value="" name="allcontacts" id="allcontacts" onchange="this.value=this.checked" />Check the box to reaffect all contacts of this company</span><br/><br/>';break;
                    }
                } else moreTxtAffect = '';
                switch(this.language) {
                    default:confirm(moreTxtAffect+'Désirez-vous réaffecter les items sélectionnés ?', fcnt, '', this.language);break;
                    case 'en':confirm(moreTxtAffect+'Do you want to reaffect selected items ?', fcnt, '', this.language);break;
                }
                break;
            case 'payer':
				switch(this.language) {
					default:confirm('Désirez-vous "Payer" les notes de frais sélectionnées ?', fcnt, '', this.language);break;
					case 'en':confirm('Do you want to "Pay" the selected expenses ?', fcnt, '', this.language);break;
				}
                break;
            case 'dupliquer':
                switch(this.language) {
                    default:confirm('Désirez-vous duppliquer les items sélectionnées ?', fcnt, '', this.language);break;
                    case 'en':confirm('Do you want to duplicate the selected items ?', fcnt, '', this.language);break;
                }
                break;
            case 'correler':
                switch(this.language) {
                    default:confirm('Désirez-vous corréler/décorréler les positionnements sélectionnés ?', fcnt, '', this.language);break;
                    case 'en':confirm('Do you want to correlate/uncorrelate the selected positioning ?', fcnt, '', this.language);break;
                }
                break;
            case 'positionner':
                this.urlComp.push('sendmail1');
                switch(type) {
                    case 'tableau-de-bord/liste-ressources':
                        this.urlComp.push('sendmail2');
                        switch(this.language) {
                            default:
                                confirm('Désirez-vous positionner la(es) ressource(s) sélectionnée(s) sur la fiche "'+reference+'" ?<div style="text-align: left;padding-left: 14%;"><br/><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail1" id="sendmail1" onchange="this.value=this.checked" />Cocher la case pour envoyer un mail au responsable de la(des) ressource(s) sélectionnée(s)</span><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail2" id="sendmail2" onchange="this.value=this.checked" />Cocher la case pour envoyer un mail au responsable du besoin concerné</span></div>', fcnt, '', this.language)
                                break;
                            case 'en':
                                confirm('Do you want to push selected resource(s) on the profile "'+reference+'" ?<div style="text-align: left;padding-left: 14%;"><br/><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail1" id="sendmail1" onchange="this.value=this.checked" />Check the box to send an email to the selected resource(s) manager</span><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail2" id="sendmail2" onchange="this.value=this.checked" />Check the box to send an email to the manager of involved opportunity</span></div>', fcnt, '', this.language);
                                break;
                        }
                        break;
                    case 'tableau-de-bord/liste-candidats':
                        this.urlComp.push('sendmail2');
                        switch(this.language) {
                            default:
                                confirm('Désirez-vous positionner le(s) candidat(s) sélectionné(s) sur la fiche "'+reference+'" ?<div style="text-align: left;padding-left: 14%;"><br/><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail1" id="sendmail1" onchange="this.value=this.checked" />Cocher la case pour envoyer un mail au responsable du(des) candidat(s) sélectionné(s)</span><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail2" id="sendmail2" onchange="this.value=this.checked" />Cocher la case pour envoyer un mail au responsable du besoin concerné</span></div>', fcnt, '', this.language)
                                break;
                            case 'en':
                                confirm('Do you want to push selected candidate(s) on the profile "'+reference+'" ?<div style="text-align: left;padding-left: 14%;"><br/><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail1" id="sendmail1" onchange="this.value=this.checked" />Check the box to send an email to the selected candidate(s) manager</span><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail2" id="sendmail2" onchange="this.value=this.checked" />Check the box to send an email to the manager of involved opportunity</span></div>', fcnt, '', this.language);
                                break;
                        }
                        break;
                    case 'tableau-de-bord/liste-besoins':
                        this.urlComp.push('sendmail2');
                        switch(this.language) {
                            default:
                                confirm('Désirez-vous positionner la fiche "'+reference+'" sur le(s) besoin(s) sélectionné(s) ?<div style="text-align: left;padding-left: 14%;"><br/><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail1" id="sendmail1" onchange="this.value=this.checked" />Cocher la case pour envoyer un mail au responsable du(des) besoin(s) sélectionné(s)</span><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail2" id="sendmail2" onchange="this.value=this.checked" />Cocher la case pour envoyer un mail au responsable du profil concerné</span></div>', fcnt, '', this.language);
                                break;
                            case 'en':
                                confirm('Do you want to push the profile "'+reference+'" on selected opportunities ?<div style="text-align: left;padding-left: 14%;"><br/><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail1" id="sendmail1" onchange="this.value=this.checked" />Check the box to send an email to the selected opportunities manager</span><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail2" id="sendmail2" onchange="this.value=this.checked" />Check the box to send an email to th involved profile manager</span></div>', fcnt, '', this.language);
                                break;
                        }
                        break;
                    case 'tableau-de-bord/liste-projets':
                        switch(this.language) {
                            default:
                                confirm('Désirez-vous positionner la fiche "'+reference+'" sur le(s) projet(s) sélectionné(s) ?<br/><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail1" id="sendmail1" onchange="this.value=this.checked" />Cocher la case pour envoyer un mail au responsable du(des) projet(s) sélectionné(s)</span>', fcnt, '', this.language);
                                break;
                            case 'en':
                                confirm('Do you want to push the profile "'+reference+'" on selected projects ?<br/><br/><span style="font-size:10px"><input type="checkbox" value="" name="sendmail1" id="sendmail1" onchange="this.value=this.checked" />Check the box to send an email to the selected projects manager</span>', fcnt, '', this.language);
                                break;
                        }
                        break;
                }
                break;
            case 'flagger':case'deflagger':
                switch(this.language) {
                    default:confirm('Désirez-vous flagger/déflagger les items sélectionnées ?', fcnt, '', this.language);break;
                    case 'en':confirm('Do you want to flag/unflag the selected items ?', fcnt, '', this.language);break;
                }
                break;
			case 'installation-api':
                switch(type) {
                    case 'tableau-de-bord/liste-clients':
                        switch(this.language) {
                            default:confirm('Désirez-vous installer l\'App "'+reference+'" au(x) client(s) sélectionné(s) ?', fcnt, '', this.language);break;
                            case 'en':confirm('Do you want to install the App "'+reference+'" to selected clients ?', fcnt, '', this.language);break;
                        }
                        break;
                }
                break;
            default:
                switch(type) {
                    case 'tableau-de-bord/fiche-commande' :this.actionItems(type+comp,method);break;
                    default:
                    	confirm(confirmMsg, fcnt, '', this.language);
                    	break;
                }
                break;
        }
    } else {
        switch(this.language) {
            default:alert('Veuillez sélectionner des items !');break;
            case 'en':alert('Please select items !');break;
        }
    }
}

/**
 * \fn actionItems
 * \brief Initialise l'action du formulaire et le soumet
 * \param nomform identifiant du formulaire
 * \param action action à réaliser
 */
Wish_Recherche.prototype.actionItems = function(action, method) {
    jQuery.each(this.urlComp, function(i, val) {action = action.concat("&"+val+"="+document.getElementById(val).value);});
    el = document.getElementById(this.id_form_resultats);
    if(method=='get' || method=='post') el.method = method;
    el.action = action;
    el.submit();
}

/**
 * \fn allSelect
 * \brief Sélectionne ou déselectionne toutes les checkbox
 * \param val valeur de la checkbox
 * \param nom des checkbox (si vide alors le nom est "check_profil[]")
 */
Wish_Recherche.prototype.allSelect = function(val) {
    var liste = document.getElementsByName(this.name_check_item);
    for(var i=0; i < liste.length; i++) liste[i].checked = val;
}

/**
 * \fn SetMailTo
 * \brief Ouvre l'application de mail par défaut et envoit un mail groupés aux profils sélectionnés
 * \param liste Tableau contenant tous les profils de la page avec leur identifiant et leur email
 * \param check_profil Nom des balises checjbox de la page
 */
Wish_Recherche.prototype.SetMailTo = function(liste) {
    var dest='';
    var liste_check = document.getElementsByName(this.name_check_item);
    for(var i=0; i < liste_check.length; i++) {
        if(liste_check.item(i).checked) {
            if(liste[liste_check.item(i).value] != '') {
                if(dest != '') dest += ',';
                dest += liste[liste_check.item(i).value];
            }
        }
    }
    if(dest != '') $(location).attr('href','mailto:'+dest);
}

/**
 * \fn updateFlag
 * \brief Affiche la boîte modale de création/modification d'un flag
 * \param url adresse web du serveur qui va traiter la requête de création/modification
 * \param libelle libellé du flag à modifier
 * \param id identifiant du libellé à modifier
 */
Wish_Recherche.prototype.updateFlag = function(url, libelle, id) {
    if(libelle==undefined) libelle='';
    if(id==undefined) id='';
    switch(this.language) {
        default:
            msgConfirm = 'Veuillez saisir le libellé du flag :<br/><br/><input id="libelle_flag" maxlength="100" class="normalinput" value="'+libelle+'" />';
            bNOK = 'Annuler';
            bOK = "Enregistrer";
            break;
        case 'en':
            msgConfirm = 'Please fill the flag wording :<br/><br/><input id="libelle_flag" maxlength="100" class="normalinput" value="'+libelle+'" />';
            bNOK = 'Cancel';
            bOK = "Save";
            break;
    }
    setMessageModal(msgConfirm+'<br /><br /><table width="100%"><tr><td width="21%">&nbsp;</td><td width="25%"><input type="button" value="'+bOK+'" onclick="'+this.varName+'.update_flag(\''+url+'\',\''+id+'\');" id="byes_modalmessage" /></td><td width="8%">&nbsp;</td><td width="25%"><input type="button" value="'+bNOK+'" onclick="closeMessageModal();" id="bno_modalmessage" /></td><td width="21%">&nbsp;</td></tr></table>');
}

/**
 * \fn update_flag
 * \brief Exécute la création du flag l'exportation
 * \param url adresse web du serveur qui va traiter la requête d'exportation
 */
Wish_Recherche.prototype.update_flag = function(url, id) {if(id==undefined) id='';var libelle = document.getElementById('libelle_flag').value;closeMessageModal();$(location).attr('href',url+'?id='+id+'&libelle_flag='+libelle);}


Wish_Recherche.prototype.addCategory = function(newCategory) {this.tabCategory.push(newCategory);}
Wish_Recherche.prototype.addItemCategory = function(category, item, newItem) {this.tabCategory[category][item].push(newItem);}

Wish_Recherche.prototype.changeModifyResults = function(label, select, button) {
    switch(this.language) {
        default:toDefine = 'A sélectionner';break;
        case 'en':toDefine = 'To select';break;
    }

    var DOMSelect = document.getElementById(select);
    //nettoyage du select
    while(DOMSelect.firstChild){
        DOMSelect.removeChild( DOMSelect.firstChild );
    }

    while(DOMSelect.options.length > 0) document.getElementById(select).options[0] = null;
    var newItem=DOMSelect.options.length;
    DOMSelect.options[newItem] = new Option('-- '+toDefine+' --', '');

    this.forCategory++;
    if(this.forCategory >= this.tabCategory.length) this.forCategory = 0;
    for(i = 0; i < this.tabCategory.length; i++) {
        if(i == this.forCategory) {
			jQuery.each(this.tabCategory[i][1], function(j, val) {
                if(val.length == 3){ // cas particulier ou les options sont regroupés
                    var optGroupName = val[2];
                    var optionLabel = val[0];
                    var optionValue = val[1];
                    // récupération de l'optgroup
                    var optg = DOMSelect.querySelector('optgroup[label="'+optGroupName+'"]');
                    // création du groupe s'il n'existe pas
                    if(!optg){
                        optg = document.createElement("optgroup");
                        optg.setAttribute('label', optGroupName);
                        DOMSelect.appendChild(optg);
                    }
                    //ajout de l'option
                    optg.appendChild( new Option(optionValue, optionLabel) );
                }else {
                    newItem = DOMSelect.options.length;
                    DOMSelect.options[newItem] = new Option(val[1], val[0]);
                }
            });
            document.getElementById(button).href = 'javascript:'+this.tabCategory[i][2];
            document.getElementById(label).innerHTML = this.tabCategory[i][0];
            break;
        }
    }

    DOMSelect.value = '';
};

Wish_Recherche.prototype.getMethodParameters = function(parameters, url, firstparam) {
	var comp_param = '';
	jQuery.each(parameters, function(i, param) {jQuery.each(param[1], function(j, val) {if(comp_param != '') comp_param += '&'+param[0]+'%5B%5D='+val; else comp_param += param[0]+'%5B%5D='+val;});});
	if(comp_param != '') {if(firstparam) window.location = url+'?'+comp_param; else window.location = url+'&'+comp_param;} else window.location = url;
};

