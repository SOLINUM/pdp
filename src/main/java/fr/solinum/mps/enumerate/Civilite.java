package fr.solinum.mps.enumerate;

public enum Civilite {

	MR("Mr"),MME("Mme"),MLLE("Mlle");
	
	private String value;

	Civilite(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return this.getValue();
	}
}
