package fr.solinum.mps.enumerate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum TypeProjet {

	ABSENCE("Absence"), INTERNE("Interne"), RJ("Régie"), FOR("Forfait");

	private String value;

	TypeProjet(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public static List<TypeProjet> valuesFiltered() {
		return Arrays.stream(TypeProjet.values()).filter(s -> !s.equals(ABSENCE) && !s.equals(INTERNE))
				.collect(Collectors.toList());
	}

}
