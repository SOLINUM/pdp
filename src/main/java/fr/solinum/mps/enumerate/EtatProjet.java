package fr.solinum.mps.enumerate;

public enum EtatProjet {

	ENCOURS("En cours"), ARCHIVE("Archivé");

	private String value;

	EtatProjet(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
