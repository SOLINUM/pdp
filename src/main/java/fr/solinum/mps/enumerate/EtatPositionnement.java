package fr.solinum.mps.enumerate;

public enum EtatPositionnement {

	POSITIONNE("Positionné"), CV_ENVOYE("CV Envoyé"), PRESENTE_CLIENT("Présenté Client"),ENTRETIEN_PART("Entretien partenaire"), REJETE("Rejeté"), GAGNE(
			"Gagné");

	private String value;

	EtatPositionnement(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
