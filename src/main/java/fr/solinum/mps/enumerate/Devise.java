package fr.solinum.mps.enumerate;

public enum Devise {

	EUR("EUR Euro"), USD("USD United States Dollars"), GPB("GPB United Kingdom Pounds");

	private String value;

	Devise(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
