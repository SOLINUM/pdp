package fr.solinum.mps.enumerate;

public enum EtatInvoice {

	PROVISOIRE("Provisoire", ""), VALIDE("Validée", "aliceblue"), ENVOYE("Envoyée", "aliceblue"), RELANCE1("Rélance 1", "orangered"), RELANCE2("Rélance 2", "orangered"), IMPAYE("Impayée", "orangered"), PAYE(
			"Payée", "greenyellow"), RECU("Récu","");

	private String value;
	private String bgColor;

	EtatInvoice(final String value, final String bgColor) {
		this.value = value;
		this.bgColor = bgColor;
	}

	public String getValue() {
		return value;
	}

	public String getBgColor() {
		return bgColor;
	}

	
}
