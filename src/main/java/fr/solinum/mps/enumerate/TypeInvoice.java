package fr.solinum.mps.enumerate;

public enum TypeInvoice {

	CLIENT("Client"), FOR("Fournisseur");
	
	private String value;

	TypeInvoice(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
