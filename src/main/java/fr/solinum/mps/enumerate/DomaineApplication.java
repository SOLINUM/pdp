package fr.solinum.mps.enumerate;

public enum DomaineApplication {

	
	DEVELOPPEUR("Développeur"), CP("Chef de projet"), PO("Product owner"), BI("Consultant BI"), DEVOPS("Devops"), SYSTEM("Ingénieur Système"), AUTRES("Autres");

	private String value;

	DomaineApplication(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
