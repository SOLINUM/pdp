package fr.solinum.mps.enumerate;

public enum StatusApplicant {

  AT("A traiter !"),
  ECQ("En cours de qualif"),
  EM("En mission"),
  VVP("Vivier++"),
  VV("Vivier"),
  NPD("Non disponible"),
  CER("Converti en Ressource"),
  EAT("Contacter plus tard"),
  VLD("Validé");


  private String value;

  StatusApplicant(final String value) {
      this.value = value;
  }

  public String getValue() {
      return value;
  }

  @Override
  public String toString() {
      return this.getValue();
  }
}

