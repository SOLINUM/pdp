package fr.solinum.mps.enumerate;

public enum EtatContact {

	PROSPECT("Prospect"), CLIENT("Client"), PARTENAIRE("Partenaire"), FOURNISSEUR("Fournisseur"), ARCHIVE("Archivé");
	
	private String value;

	EtatContact(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
