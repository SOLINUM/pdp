package fr.solinum.mps.enumerate;

public enum StatusCRA {

	NOUVEAU("Nouveau"), BROUILLON("Ouvert"), CONFIRM("Confirmé"),APPROUVE("Approuvé"),REJETE("Rejeté");

	private String value;

	StatusCRA(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
