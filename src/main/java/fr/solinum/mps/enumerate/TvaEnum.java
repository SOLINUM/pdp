package fr.solinum.mps.enumerate;

public enum TvaEnum {
	
	TVA_20("20%"), TVA_10("10%"), TVA_0("0%");

	private String value;

	TvaEnum(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
