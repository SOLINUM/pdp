package fr.solinum.mps.enumerate;

public enum Provenance {

	INDIFF("Indifférent"), PROSPECTION("Prospection"), APPORTEUR("Apporteur"), COLLEGUE("Collègue"), RESEAU("Réseau"),
	APPEL_OFFRE("Appel d'offre"), APPEL_ENTRANT("Appel entrant"), CLIENT("client"), SALON("Salon"), GOOGLE("Google"), LINKEDIN("LinkedIn");
	
	private String value;

	Provenance(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
