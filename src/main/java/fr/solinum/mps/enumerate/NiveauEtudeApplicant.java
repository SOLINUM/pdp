package fr.solinum.mps.enumerate;

public enum NiveauEtudeApplicant {

  LICENCE,
  MAITRISE,
  DOCTORAT,
  MASTERE,
  INGENIEUR;

}

