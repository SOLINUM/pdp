package fr.solinum.mps.enumerate;

public enum ModeReglementFacture {

	
	ND("Non défini"), VIREMENT("Virement"), PRELEVEMENT("Prélevement"), CHEQUE("Chèque"), CB("CB");

	private String value;

	ModeReglementFacture(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
