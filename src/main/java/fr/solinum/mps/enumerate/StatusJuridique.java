package fr.solinum.mps.enumerate;

public enum StatusJuridique {

	EI("entreprise individuelle"), EIRL("EIRL"), SARL("SARL"), EURL("EURL"), SAS("SAS"), SASU("SASU"), SA("SA"), SNC(
			"SNC");
	private String value;

	StatusJuridique(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}
