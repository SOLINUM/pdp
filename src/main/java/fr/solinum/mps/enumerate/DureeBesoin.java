package fr.solinum.mps.enumerate;

public enum DureeBesoin {

	MOIN_1MOIS("<1 mois"), UN_MOIS("1 mois"), DEUX_MOIS("2 mois"), TROIS_MOIS("3 mois"), QUATRE_MOIS(
			"4 mois"), CINQ_MOIS("5 mois"), SIX_MOIS("6 mois"), SUPP_6MOIS(" > 6 mois"), INDET("Indéterminée");

	private String value;

	DureeBesoin(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
