package fr.solinum.mps.enumerate;

public enum SecteurActiviteEntreprise {

    autres("Autres"),
    aeronautique("Aéronautique"),
    aerospatial("Aérospatial"),
    agroalimentaire("Agroalimentaire"),
    assurance("Assurance"),
    automobile("Automobile"),
    banque("Banque"),
    batiments("Bâtiments"),
    biomedical("Biomédical"),
    chimie("Chimie"),
    conseil("Conseil"),
    defense("Défense"),
    editiondelogiciels("Edition de logiciels"),
    energie("Energie"),
    environnement("Environnement"),
    ferroviaire("Ferroviaire"),
    grandedistribution("Grande distribution"),
    infrastructure("Infrastructure"),
    logistique("Logistique"),
    media("Média"),
    metallurgiesiderurgie("Métallurgie/sidérurgie"),
    naval("Naval"),
    nucleaire("Nucléaire"),
    oilgaz("Oil &amp; Gaz"),
    petrochimie("Pétrochimie"),
    pharmacie("Pharmacie"),
    sante("Santé"),
    secteurpublic("Secteur public"),
    services("Services"),
    telecommunications("Télécommunications");

	private String value;

	SecteurActiviteEntreprise(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}
