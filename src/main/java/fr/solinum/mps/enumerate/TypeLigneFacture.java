package fr.solinum.mps.enumerate;

public enum TypeLigneFacture {
	
	NORMALE("Normale"), ND ("Non défini"), ECHANCE("Echance"), FRF("Frais réfacturé"), ARF("Achat réfacturé");

	private String value;

	TypeLigneFacture(final String value) {
	      this.value = value;
	  }

	public String getValue() {
		return value;
	}
}
