package fr.solinum.mps.enumerate;

public enum TypeBesoin {

	
	REGIE("Régie"), FORFAIT("Forfait") , RECRUTEMENT("Recrutement") , PRODUIT("Produit") , PROJETINTERNE("Projet interne") ;

	private String value;

	TypeBesoin(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
