package fr.solinum.mps.enumerate;

public enum EtatBesoin {

	ENCOURS("En cours"), REPORTE("Reporté"), GAGNE("Gagné"), PERDU("Perdu"), ABONDONNE("Abondonée");

	private String value;

	EtatBesoin(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
