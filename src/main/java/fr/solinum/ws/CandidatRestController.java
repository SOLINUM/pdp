package fr.solinum.ws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.solinum.entities.mps.Applicant;
import fr.solinum.repository.mps.IApplicantRepository;
import fr.solinum.utils.ContratConstants;

@RestController
public class CandidatRestController {
	@Autowired
	IApplicantRepository applicantRepository;
	@GetMapping("/mps/api/candidat/search")
public List<Map<String, Object>> getApplicantByName(@RequestParam(name = "q")String queryString){
   	List<Applicant> list = applicantRepository.findByNameContaining(queryString);
	List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
	for (Applicant applicant : list) {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("name", applicant.getName());
		map.put("dateDisponibilite", applicant.getDateDisponibilite());
		map.put("disponibilite", applicant.getDisponibilite());
		map.put("state", applicant.getState());
		String typeContrat = applicant.getTypeContrat();
		map.put("tyepContrat", applicant.getTypeContrat());
		if (typeContrat.equals(ContratConstants.CDI) || typeContrat.equals(ContratConstants.CDD)) {
			String salairePropose=applicant.getSalairePropose();
			if(salairePropose!=null)map.put("salairePropose", salairePropose);
			else map.put("salaireDemande", applicant.getSalaireDemande());
		} else {
			if(typeContrat.equals(ContratConstants.FRL) || typeContrat.equals(ContratConstants.STT)||typeContrat.equals(ContratConstants.PORT))
			{
				String TjmPropose=applicant.getTjmPropose();
				if(TjmPropose!=null) map.put("tjmPropose", TjmPropose);
				else map.put("tjmDemande", applicant.getTjmDemande());
			}
		}

		results.add(map);
	}
	return results;
}
}
