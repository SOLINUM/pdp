package fr.solinum.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "fr.solinum.repository.mps",
        entityManagerFactoryRef = "mpsEntityManagerFactory",
        transactionManagerRef= "mpsTransactionManager"
)
public class MpsDataSourceConfiguration {
    @Bean
    @ConfigurationProperties("mps.datasource")
    public DataSourceProperties mpsDataSourceProperties() {
        return new DataSourceProperties();
    }
    @Bean
    @ConfigurationProperties("mps.datasource.configuration")
    public DataSource mpsDataSource() {
        return mpsDataSourceProperties().initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();
    }
   
    @Bean(name = "mpsEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean mpsEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(mpsDataSource())
                .packages("fr.solinum.entities.mps")
                .build();
    }
    
    @Bean
    public PlatformTransactionManager mpsTransactionManager(
            final @Qualifier("mpsEntityManagerFactory") LocalContainerEntityManagerFactoryBean mpsEntityManagerFactory) {
        return new JpaTransactionManager(mpsEntityManagerFactory.getObject());
    }

}