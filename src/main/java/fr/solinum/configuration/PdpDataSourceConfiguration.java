package fr.solinum.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "fr.solinum.repository.pdp",
        entityManagerFactoryRef = "pdpEntityManagerFactory",
        transactionManagerRef= "pdpTransactionManager"
)
public class PdpDataSourceConfiguration {
    @Bean
    @Primary
    @ConfigurationProperties("pdp.datasource")
    public DataSourceProperties pdpDataSourceProperties() {
        return new DataSourceProperties();
    }
    @Bean
    @Primary
    @ConfigurationProperties("pdp.datasource.configuration")
    public DataSource pdpDataSource() {
        return pdpDataSourceProperties().initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();
    }
    @Primary
    @Bean(name = "pdpEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean pdpEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(pdpDataSource())
                .packages("fr.solinum.entities.pdp")
                .build();
    }
    @Primary
    @Bean
    public PlatformTransactionManager pdpTransactionManager(
            final @Qualifier("pdpEntityManagerFactory") LocalContainerEntityManagerFactoryBean pdpEntityManagerFactory) {
        return new JpaTransactionManager(pdpEntityManagerFactory.getObject());
    }

}
