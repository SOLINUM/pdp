package fr.solinum.entities.mps;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import fr.solinum.mps.enumerate.EtatInvoice;
import fr.solinum.mps.enumerate.ModeReglementFacture;
import fr.solinum.mps.enumerate.TypeInvoice;

@Entity
@Table(name = "invoice")
public class Invoice {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "reference")
	private String reference;

	@Column(name = "montantht")
	private Float montantHt;

	@Column(name = "montantttc")
	private Float montantTTc;

	@Column(name = "tva")
	private Float tva;

	@OneToOne
	private Cra cra;

	@ManyToOne
	private Projet projet;
	
	@ManyToOne
	private Applicant applicant;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_facturation")
	private Date dateFacturation;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "date_reglement_prevu")
	private Date dateReglementPrevu;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "date_reglement")
	private Date dateReglemement;

	@Enumerated(EnumType.STRING)
	@Column(name = "etat_facture")
	private EtatInvoice etatFacture;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "type_facture")
	private TypeInvoice typeInvoice;

	@Enumerated(EnumType.STRING)
	@Column(name = "mode_reglement_facture")
	private ModeReglementFacture modeReglementFacture;

	@Column(name = "taux_remise")
	private Float tauxRemise;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "date_debut")
	private Date dateDebut;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "date_fin")
	private Date dateFin;

	@Column(name = "commentaires")
	private String commentaires;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "date_modification")
	@UpdateTimestamp
	private Date dateModification;

	@OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<InvoiceLine> invoiceLines;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Date getDateFacturation() {
		return dateFacturation;
	}

	public void setDateFacturation(Date dateFacturation) {
		this.dateFacturation = dateFacturation;
	}

	public Date getDateReglementPrevu() {
		return dateReglementPrevu;
	}

	public void setDateReglementPrevu(Date dateReglementPrevu) {
		this.dateReglementPrevu = dateReglementPrevu;
	}

	public Date getDateReglemement() {
		return dateReglemement;
	}

	public void setDateReglemement(Date dateReglemement) {
		this.dateReglemement = dateReglemement;
	}

	public EtatInvoice getEtatFacture() {
		return etatFacture;
	}

	public void setEtatFacture(EtatInvoice etatFacture) {
		this.etatFacture = etatFacture;
	}

	public ModeReglementFacture getModeReglementFacture() {
		return modeReglementFacture == null ? ModeReglementFacture.ND : modeReglementFacture;
	}

	public void setModeReglementFacture(ModeReglementFacture modeReglementFacture) {
		this.modeReglementFacture = modeReglementFacture;
	}

	public Float getTauxRemise() {
		return tauxRemise;
	}

	public void setTauxRemise(Float tauxRemise) {
		this.tauxRemise = tauxRemise;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public String getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public Float getMontantHt() {
		return montantHt;
	}

	public void setMontantHt(Float montantHt) {
		this.montantHt = montantHt;
		if (montantHt != null) {
			this.montantTTc = this.montantHt * (1 + getTva());
		}
	}

	public Float getMontantTTc() {
		return montantTTc;
	}

	public Float getTva() {
		if (tva == null) {
			return 0.2f;
		}
		return tva;
	}

	public void setTva(Float tva) {
		this.tva = tva;
	}

	public Cra getCra() {
		return cra;
	}

	public void setCra(Cra cra) {
		this.cra = cra;
	}

	public Projet getProjet() {
		return projet;
	}

	public void setProjet(Projet projet) {
		this.projet = projet;
	}

	public List<InvoiceLine> getInvoiceLines() {
		if (invoiceLines == null) {
			invoiceLines = new ArrayList<InvoiceLine>();
		}
		return invoiceLines;
	}

	public void setInvoiceLines(List<InvoiceLine> invoiceLines) {
		this.invoiceLines = invoiceLines;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public TypeInvoice getTypeInvoice() {
		if (typeInvoice==null){
			typeInvoice = TypeInvoice.CLIENT;
		}
		return typeInvoice;
	}

	public void setTypeInvoice(TypeInvoice typeInvoice) {
		if (typeInvoice==null){
			typeInvoice = TypeInvoice.CLIENT;
		}
		this.typeInvoice = typeInvoice;
	}

}
