package fr.solinum.entities.mps;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import fr.solinum.mps.enumerate.Devise;
import fr.solinum.mps.enumerate.EtatProjet;
import fr.solinum.mps.enumerate.TypeProjet;

@Entity
@Table(name = "projet")
public class Projet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String reference;

	private String lp_adresse;

	private String lp_cp;

	private String lp_ville;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_debut")
	private Date dateDebut;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_fin")
	private Date dateFin;

	@Enumerated(EnumType.STRING)
	@Column(name = "devise")
	private Devise devise;

	@Enumerated(EnumType.STRING)
	@Column(name = "type_projet")
	private TypeProjet typeProjet;

	@Enumerated(EnumType.STRING)
	@Column(name = "etat_projet")
	private EtatProjet etatProjet;
	
	@Column(name = "numero_commande")
	private String numeroCommande;
	
	@OneToOne
	@JoinColumn(name="contact_id")
	private Contact contact;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modification")
	@UpdateTimestamp
	private Date dateModification;

	@CreationTimestamp
	@Column(name = "date_creation")
	private Date dateCreation;
	
	@OneToMany(mappedBy = "projet", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ProjetApplicant> projetApplicants = new ArrayList<>();

	private String commentaire;
	
	@OneToMany(mappedBy = "projet")
	private List<Invoice> invoices;

	@Transient
	private String contactName;

	@Transient
	private String ressourcesName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getLp_adresse() {
		return lp_adresse;
	}

	public void setLp_adresse(String lp_adresse) {
		this.lp_adresse = lp_adresse;
	}

	public String getLp_cp() {
		return lp_cp;
	}

	public void setLp_cp(String lp_cp) {
		this.lp_cp = lp_cp;
	}

	public String getLp_ville() {
		return lp_ville;
	}

	public void setLp_ville(String lp_ville) {
		this.lp_ville = lp_ville;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public Devise getDevise() {
		return devise;
	}

	public void setDevise(Devise devise) {
		this.devise = devise;
	}

	public TypeProjet getTypeProjet() {
		return typeProjet;
	}

	public void setTypeProjet(TypeProjet typeProjet) {
		this.typeProjet = typeProjet;
	}

	public EtatProjet getEtatProjet() {
		return etatProjet;
	}

	public void setEtatProjet(EtatProjet etatProjet) {
		this.etatProjet = etatProjet;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public List<ProjetApplicant> getProjetApplicants() {
		return projetApplicants;
	}

	public void setProjetApplicants(List<ProjetApplicant> projetApplicants) {
		this.projetApplicants = projetApplicants;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getRessourcesName() {
		return ressourcesName;
	}

	public void setRessourcesName(String ressourcesName) {
		this.ressourcesName = ressourcesName;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public List<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<Invoice> invoices) {
		this.invoices = invoices;
	}

	public String getNumeroCommande() {
		return numeroCommande;
	}

	public void setNumeroCommande(String numeroCommande) {
		this.numeroCommande = numeroCommande;
	}

	@Override
	public String toString() {
		return (reference != null ? reference + "-" : "") + (typeProjet != null ? typeProjet.getValue() : "");
	}

	public boolean isFacturable() {
		return TypeProjet.RJ.equals(typeProjet) ||  TypeProjet.FOR.equals(typeProjet);
	}

	
}