package fr.solinum.entities.mps;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import fr.solinum.mps.enumerate.Civilite;
import fr.solinum.mps.enumerate.Provenance;

@Entity
@Table(name = "interlocuteurcontact")
public class InterlocuteurContact {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String firstname;
	private String lastname;
	@Enumerated(EnumType.STRING)
	private Civilite civilite;
	private String fonction;
	private String service;
	@ManyToOne
	private User influenceur;
	@Enumerated(EnumType.STRING)
	private Provenance provenanceContact;
	private String email1;
	private String email2;
	private String email3;
	private String telephone1;
	private String telephone2;
	private String fax;
	private String address;
	@Column(name = "code_postale")
	private Integer codePostale;
	private String ville;
	@Column(name = "perimetre_technique", columnDefinition = "MEDIUMTEXT")
	private String perimetreTechnique;

	@CreationTimestamp
	@Column(name = "date_creation")
	private Date dateCreation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modification")
	@UpdateTimestamp
	private Date dateModification;

	@ManyToOne
	private Contact contact;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public User getInfluenceur() {
		return influenceur;
	}

	public void setInfluenceur(User influenceur) {
		this.influenceur = influenceur;
	}

	public Provenance getProvenanceContact() {
		return provenanceContact;
	}

	public void setProvenanceContact(Provenance provenanceContact) {
		this.provenanceContact = provenanceContact;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getEmail3() {
		return email3;
	}

	public void setEmail3(String email3) {
		this.email3 = email3;
	}

	public String getTelephone1() {
		return telephone1;
	}

	public void setTelephone1(String telephone1) {
		this.telephone1 = telephone1;
	}

	public String getTelephone2() {
		return telephone2;
	}

	public void setTelephone2(String telephone2) {
		this.telephone2 = telephone2;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getCodePostale() {
		return codePostale;
	}

	public void setCodePostale(Integer codePostale) {
		this.codePostale = codePostale;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPerimetreTechnique() {
		return perimetreTechnique;
	}

	public void setPerimetreTechnique(String perimetreTechnique) {
		this.perimetreTechnique = perimetreTechnique;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public String getAllEmails() {
		String emails = getEmail1() == null ? ""
				: getEmail1().concat(",") + getEmail2() == null ? ""
						: getEmail2().concat(",") + getEmail3() == null ? "" : getEmail3();
		return emails;
	}

}
