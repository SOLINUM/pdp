package fr.solinum.entities.mps;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import fr.solinum.mps.enumerate.EtatPositionnement;

@Entity
@Table(name = "positionnement")
public class Positionnement {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modification")
	@UpdateTimestamp
	private Date dateModification;

	@CreationTimestamp
	@Column(name = "date_creation")
	private Date dateCreation;

	@Enumerated(EnumType.STRING)
	@Column(name = "etat_positionnement")
	private EtatPositionnement etatPositionnement;

	@ManyToOne
	private Besoin besoin;

	@ManyToOne
	private Applicant applicant;

	@ManyToOne
	private User positionePar;
	
	private String tjm;
	
	@Transient
	private String lastNote;
	
	@OneToMany(mappedBy = "positionnement", cascade = CascadeType.ALL)
	private List<NotePositionnement> notes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public EtatPositionnement getEtatPositionnement() {
		return etatPositionnement;
	}

	public void setEtatPositionnement(EtatPositionnement etatPositionnement) {
		this.etatPositionnement = etatPositionnement;
	}

	public Besoin getBesoin() {
		return besoin;
	}

	public void setBesoin(Besoin besoin) {
		this.besoin = besoin;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public User getPositionePar() {
		return positionePar;
	}

	public void setPositionePar(User positionePar) {
		this.positionePar = positionePar;
	}

	public List<NotePositionnement> getNotes() {
		return notes;
	}

	public void setNotes(List<NotePositionnement> notes) {
		this.notes = notes;
	}

	public String getTjm() {
		return tjm;
	}

	public void setTjm(String tjm) {
		this.tjm = tjm;
	}

	public String getLastNote() {
		return lastNote;
	}

	public void setLastNote(String lastNote) {
		this.lastNote = lastNote;
	}
}
