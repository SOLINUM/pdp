package fr.solinum.entities.mps;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import fr.solinum.mps.enumerate.Provenance;
import fr.solinum.mps.enumerate.StatusApplicant;

/**
 * The persistent class for the job database table.
 * 
 */
@Entity
@Table(name = "job")
@NamedQuery(name = "Job.findAll", query = "SELECT j FROM Job j")
public class Job implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	public enum Status {
		ENCOURS, TERMINE, REJETE;
	}

	@Id
	// @SequenceGenerator(name="job_id_seq", sequenceName="job_id_seq",
	// allocationSize = 1)
	// @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="job_id_seq")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String description;

	private Integer salary;

	@Size(min = 3, max = 150, message = "Le Titre doit être comprise entre 3 et 255 caractères")
	@NotEmpty(message = "Le Titre ne peut pas être vide")
	@NotNull(message = "Le Titre ne peut pas être null")
	private String title;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_posted")
	private Date datePosted;

	@Enumerated(EnumType.STRING)
	private Status status;

	private String email;

	@Column(name = "nb_employe_prevu")
	private Integer nbEmployePrevu;

	@ManyToOne
	@JoinColumn(name = "contact_id")
	private Contact contact;

	@Column(name = "departement")
	private String departement;

	@Column(name = "etat_envoi", columnDefinition = "VARCHAR(45) default 'NOUVEAU'")
	private String etatEnvoi;

	// bi-directional many-to-one association to JobType
	@ManyToOne
	@JoinColumn(name = "job_type_id")
	private JobType jobType;

	// bi-directional many-to-one association to JobApplicant
	@OneToMany(mappedBy = "job", cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<JobApplicant> jobApplicants;

	@OneToMany
	@JoinColumn(name = "job_id")
	private List<Document> documents;

	@ManyToOne
	private User responsable;

	@Enumerated(EnumType.STRING)
	@Column(name = "provenance")
	private Provenance provenance;
	
	@Column(name = "adresse")
	private String adresse;

	@Column(name = "cp")
	private String cp;

	@Column(name = "ville")
	private String ville;
	
	@Column(name = "nombre_experience")
	private String nombreExperience;
	
	@Column(name = "tjm")
	private String tjm;
	
	@Column(name = "competences_indispensable")
	private String competencesIndispensable;
	
	@Column(name = "commentaire")
	private String commentaire;
	
	@Column(name = "mobilite")
	private String mobilite;
	
	@Transient
	private String contactName;
	
	@OneToOne
	@JoinColumn(name = "besoin_id")
	private Besoin besoin;

	@Transient
	private List<JobApplicant> jobApplicantsRecruited;

	@Transient
	private List<JobApplicant> jobApplicantsEncours;

	@Transient
	private List<Document> jobDocuments;

	public Job() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSalary() {
		return this.salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public JobType getJobType() {
		return this.jobType;
	}

	public void setJobType(JobType jobType) {
		this.jobType = jobType;
	}

	public List<JobApplicant> getJobApplicants() {
		List<JobApplicant> listFilterd = jobApplicants.stream()
				.filter(line -> !"SUPPRIME".equals(line.getApplicant().getEtatEnvoi())).collect(Collectors.toList());
		return listFilterd;
	}

	public void setJobApplicants(List<JobApplicant> jobApplicants) {
		this.jobApplicants = jobApplicants;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getNbEmployePrevu() {
		return nbEmployePrevu;
	}

	public void setNbEmployePrevu(Integer nbEmployePrevu) {
		this.nbEmployePrevu = nbEmployePrevu;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public String getDepartement() {
		return departement;
	}

	public void setDepartement(String departement) {
		this.departement = departement;
	}

	public Date getDatePosted() {
		return datePosted;
	}

	public void setDatePosted(Date datePosted) {
		this.datePosted = datePosted;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public JobApplicant addJobApplicant(JobApplicant jobApplicant) {
		getJobApplicants().add(jobApplicant);
		jobApplicant.setJob(this);

		return jobApplicant;
	}

	public JobApplicant removeJobApplicant(JobApplicant jobApplicant) {
		getJobApplicants().remove(jobApplicant);
		jobApplicant.setJob(null);

		return jobApplicant;
	}

	public List<JobApplicant> getJobApplicantsRecruited() {
		jobApplicantsRecruited = getJobApplicants().stream()
				.filter(line -> StatusApplicant.CER.getValue().equals(line.getApplicant().getState()))
				.collect(Collectors.toList());
		return jobApplicantsRecruited;
	}

	public void setJobApplicantsRecruited(List<JobApplicant> jobApplicantsRecruited) {
		this.jobApplicantsRecruited = jobApplicantsRecruited;
	}

	public List<JobApplicant> getJobApplicantsEncours() {
		jobApplicantsEncours = getJobApplicants().stream()
				.filter(line -> !StatusApplicant.CER.getValue().equals(line.getApplicant().getState()))
				.collect(Collectors.toList());
		return jobApplicantsEncours;
	}

	public void setJobApplicantsEncours(List<JobApplicant> jobApplicantsEncours) {
		this.jobApplicantsEncours = jobApplicantsEncours;
	}

	@Override
	public String toString() {
		return "Job [id=" + id + ", description=" + description + ", salary=" + salary + ", title=" + title
				+ ", datePosted=" + datePosted + ", status=" + status + ", jobType="
				+ ((jobType == null) ? null : jobType.getId()) + "]";
	}

	public String toStringForList() {
		return getTitle() + " " + (getDepartement() == null ? "" : " - " + getDepartement());
	}

	public String getEtatEnvoi() {
		return etatEnvoi;
	}

	public void setEtatEnvoi(String etatEnvoi) {
		this.etatEnvoi = etatEnvoi;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public List<Document> getJobDocuments() {
		jobDocuments = getDocuments().stream().filter(line -> !"SUPPRIME".equals(line.getEtatEnvoi()))
				.collect(Collectors.toList());
		return jobDocuments;
	}

	public void setJobDocuments(List<Document> jobDocuments) {
		this.jobDocuments = jobDocuments;
	}

	public User getResponsable() {
		return responsable;
	}

	public void setResponsable(User responsable) {
		this.responsable = responsable;
	}

	public Provenance getProvenance() {
		return provenance;
	}

	public void setProvenance(Provenance provenance) {
		this.provenance = provenance;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getNombreExperience() {
		return nombreExperience;
	}

	public void setNombreExperience(String nombreExperience) {
		this.nombreExperience = nombreExperience;
	}

	public String getTjm() {
		return tjm;
	}

	public void setTjm(String tjm) {
		this.tjm = tjm;
	}

	public String getCompetencesIndispensable() {
		return competencesIndispensable;
	}

	public void setCompetencesIndispensable(String competencesIndispensable) {
		this.competencesIndispensable = competencesIndispensable;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getMobilite() {
		return mobilite;
	}

	public void setMobilite(String mobilite) {
		this.mobilite = mobilite;
	}

	public Besoin getBesoin() {
		return besoin;
	}

	public void setBesoin(Besoin besoin) {
		this.besoin = besoin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datePosted == null) ? 0 : datePosted.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((jobApplicants == null) ? 0 : jobApplicants.hashCode());
		result = prime * result + ((jobType == null) ? 0 : jobType.hashCode());
		result = prime * result + ((salary == null) ? 0 : salary.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Job other = (Job) obj;
		if (datePosted == null) {
			if (other.datePosted != null)
				return false;
		} else if (!datePosted.equals(other.datePosted))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (jobApplicants == null) {
			if (other.jobApplicants != null)
				return false;
		} else if (!jobApplicants.equals(other.jobApplicants))
			return false;
		if (jobType == null) {
			if (other.jobType != null)
				return false;
		} else if (!(jobType.getId() == other.jobType.getId()))
			return false;
		if (salary == null) {
			if (other.salary != null)
				return false;
		} else if (!salary.equals(other.salary))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}