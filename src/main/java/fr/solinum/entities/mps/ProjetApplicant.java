package fr.solinum.entities.mps;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "applicants_projets")
public class ProjetApplicant {

	@EmbeddedId
	private ProjetApplicantId projetApplicantId;

	@ManyToOne
	@MapsId("projetId")
	private Projet projet;
	
	@ManyToOne
	@MapsId("applicantId")
	private Applicant applicant;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_debut_positionnement")
	private Date dateDebutPositionnement;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_fin_positionnement")
	private Date dateFinPositionnement;
	
	private Float tjm;
	
	private Float caHt;
	
	private Float cout;
	
	public ProjetApplicant(){
		super();
	}

	public ProjetApplicant(ProjetApplicantId projetApplicantId, Projet projet, Applicant applicant, Date dateDebutPositionnement,
			Date dateFinPositionnement, Float tjm, Float caHt, Float cout) {
		super();
		this.projetApplicantId = projetApplicantId;
		this.projet = projet;
		this.applicant = applicant;
		this.dateDebutPositionnement = dateDebutPositionnement;
		this.dateFinPositionnement = dateFinPositionnement;
		this.tjm = tjm;
		this.caHt = caHt;
		this.cout = cout;
	}

	public ProjetApplicantId getProjetApplicantId() {
		return projetApplicantId;
	}

	public void setProjetApplicantId(ProjetApplicantId projetApplicantId) {
		this.projetApplicantId = projetApplicantId;
	}

	public Projet getProjet() {
		return projet;
	}

	public void setProjet(Projet projet) {
		this.projet = projet;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public Date getDateDebutPositionnement() {
		return dateDebutPositionnement;
	}

	public void setDateDebutPositionnement(Date dateDebutPositionnement) {
		this.dateDebutPositionnement = dateDebutPositionnement;
	}

	public Date getDateFinPositionnement() {
		return dateFinPositionnement;
	}

	public void setDateFinPositionnement(Date dateFinPositionnement) {
		this.dateFinPositionnement = dateFinPositionnement;
	}

	public Float getTjm() {
		return tjm;
	}

	public void setTjm(Float tjm) {
		this.tjm = tjm;
	}

	public Float getCaHt() {
		return caHt;
	}

	public void setCaHt(Float caHt) {
		this.caHt = caHt;
	}

	public Float getCout() {
		return cout;
	}

	public void setCout(Float cout) {
		this.cout = cout;
	}
	
	
}
