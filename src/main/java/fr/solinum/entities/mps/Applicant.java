package fr.solinum.entities.mps;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import fr.solinum.mps.enumerate.NiveauEtudeApplicant;



@Entity
@Table(name = "applicant")
public class Applicant {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String address;

	private String cp;

	private String ville;

	private String pays;

	@Column(name = "lieu_naissance")
	private String lieuNaissance;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_naissance")
	private Date dateNaissance;

	private String nationalite;

	@Size(message = "NIR ne peut pas dépasser 13 caractères", max = 13, min = 0)
	private String nir;

	@Column(name = "situation_familiale")
	private String situationFamiliale;

	@Size(message = "E-mail ne peut pas dépasser 45 caractères", max = 45)
	@Email(message = "E-mail doit être une adresse valide")
	// @Unique(service = ApplicantService.class, fieldName = "email", message =
	// "E-mail déja existe !")
	private String email;

	@NotEmpty(message = "Civilité ne peut pas être vide")
	private String civilite;

	@Size(message = "Nom ne peut pas dépasser 100 caractères", max = 100)
	@NotEmpty(message = "Nom ne peut pas être vide")
	// @Unique(service = ApplicantService.class, fieldName = "name", message =
	// "Nom déja existe !")
	private String name;

	@Size(max = 100, min = 2, message = "Statut ne peut être vide")
	private String state;

	@Size(message = "Titre ne peut pas dépasser 100 caractères", max = 100)
	@NotEmpty(message = "title cannot be empty")
	private String title;

	// @NotEmpty(message = "Mobile ne peut pas être vide")
	// @Unique(service = ApplicantService.class, fieldName = "mobile", message =
	// "Téléphone déja existe !")
	private String mobile;

	@Enumerated(EnumType.STRING)
	@Column(name = "niveau_etude")
	private NiveauEtudeApplicant niveauEtude;

	@Column(name = "source")
	private String source;

	@Column(name = "salaire_demande")
	private String salaireDemande;

	@Column(name = "salaire_propose")
	private String salairePropose;

	@Column(name = "tjm_demande")
	private String tjmDemande;

	@Column(name = "tjm_propose")
	private String tjmPropose;

	// @Size(message = "Résumé peut pas dépasser 255 caractères", max = 255)
	@Column(columnDefinition = "MEDIUMTEXT")
	private String note;

	@Column(columnDefinition = "MEDIUMTEXT",name="dossier_technique")
	private String dossierTechnique;

	@CreationTimestamp
	@Column(name = "date_creation")
	private Date dateCreation;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_disponibilite")
	private Date dateDisponibilite;

	@Column(name = "etat_envoi", columnDefinition = "VARCHAR(45) default 'NOUVEAU'")
	private String etatEnvoi;

	@Size(max = 100, min = 2, message = "Type contrat ne peut être vide")
	@Column(name = "type_contrat")
	private String typeContrat;

	@Column(name = "disponibilite")
	private String disponibilite;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modification")
	@UpdateTimestamp
	private Date dateModification;

	@Column(name = "evaluation")
	private String evaluation;

	@Column(name = "experience")
	private String experience;

	@Column(name = "mobilite")
	private String mobilite;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_premiere_experience")
	private Date datePremiereExperience;

	@ManyToOne
	private User user;

	@OneToMany(mappedBy = "applicant", cascade = CascadeType.ALL)
	//@JsonManagedReference
	@JsonIgnore
	private List<JobApplicant> jobApplicants;

	@OneToMany
	@JsonIgnore
	private List<Task> tasks;

	@OneToMany(mappedBy = "applicant", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnore
	private List<ProjetApplicant> projetsApplicant = new ArrayList<>();

	@Transient
	private Job jobSelected;
	
	private boolean guest;

	public Applicant(String email, String name, String state, String title, String mobile,
			NiveauEtudeApplicant niveauEtude, String source, String salaireDemande, String salairePropose, String note,
			Date dateDisponibilite, String etatEnvoi, User user, List<JobApplicant> jobApplicants, Job jobSelected) {
		super();
		this.email = email;
		this.name = name;
		this.state = state;
		this.title = title;
		this.mobile = mobile;
		this.niveauEtude = niveauEtude;
		this.source = source;
		this.salaireDemande = salaireDemande;
		this.salairePropose = salairePropose;
		this.note = note;
		this.dateDisponibilite = dateDisponibilite;
		this.etatEnvoi = etatEnvoi;
		this.user = user;
		this.jobApplicants = jobApplicants;
		this.jobSelected = jobSelected;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<JobApplicant> getJobApplicants() {
		return jobApplicants;
	}

	public void setJobApplicants(List<JobApplicant> jobApplicants) {
		this.jobApplicants = jobApplicants;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public NiveauEtudeApplicant getNiveauEtude() {
		return niveauEtude;
	}

	public void setNiveauEtude(NiveauEtudeApplicant niveauEtude) {
		this.niveauEtude = niveauEtude;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSalaireDemande() {
		return salaireDemande;
	}

	public void setSalaireDemande(String salaireDemande) {
		this.salaireDemande = salaireDemande;
	}

	public String getSalairePropose() {
		return salairePropose;
	}

	public void setSalairePropose(String salairePropose) {
		this.salairePropose = salairePropose;
	}

	public String getTjmDemande() {
		return tjmDemande;
	}

	public void setTjmDemande(String tjmDemande) {
		this.tjmDemande = tjmDemande;
	}

	public String getTjmPropose() {
		return tjmPropose;
	}

	public void setTjmPropose(String tjmPropose) {
		this.tjmPropose = tjmPropose;
	}

	public Date getDatePremiereExperience() {
		return datePremiereExperience;
	}

	public void setDatePremiereExperience(Date datePremiereExperience) {
		this.datePremiereExperience = datePremiereExperience;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateDisponibilite() {
		return dateDisponibilite;
	}

	public void setDateDisponibilite(Date dateDisponibilite) {
		this.dateDisponibilite = dateDisponibilite;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Applicant() {
		this.etatEnvoi = "NOUVEAU";
	}

	public Job getJobSelected() {
		return jobSelected;
	}

	public void setJobSelected(Job jobSelected) {
		this.jobSelected = jobSelected;
	}

	public String getEtatEnvoi() {
		return etatEnvoi;
	}

	public void setEtatEnvoi(String etatEnvoi) {
		this.etatEnvoi = etatEnvoi;
	}

	public String getTypeContrat() {
		return typeContrat;
	}

	public void setTypeContrat(String typeContrat) {
		this.typeContrat = typeContrat;
	}

	public String getDisponibilite() {
		return disponibilite;
	}

	public void setDisponibilite(String disponibilite) {
		this.disponibilite = disponibilite;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public String getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}

	public String getLieuNaissance() {
		return lieuNaissance;
	}

	public void setLieuNaissance(String lieuNaissance) {
		this.lieuNaissance = lieuNaissance;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getNationalite() {
		return nationalite;
	}

	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}

	public String getNir() {
		return nir;
	}

	public void setNir(String nir) {
		this.nir = nir;
	}

	public String getSituationFamiliale() {
		return situationFamiliale;
	}

	public void setSituationFamiliale(String situationFamiliale) {
		this.situationFamiliale = situationFamiliale;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getMobilite() {
		return mobilite;
	}

	public void setMobilite(String mobilite) {
		this.mobilite = mobilite;
	}

	public String getDossierTechnique() {
		return dossierTechnique;
	}

	public void setDossierTechnique(String dossierTechnique) {
		this.dossierTechnique = dossierTechnique;
	}

	public List<ProjetApplicant> getProjetsApplicant() {
		return projetsApplicant;
	}

	public void setProjetsApplicant(List<ProjetApplicant> projetsApplicant) {
		this.projetsApplicant = projetsApplicant;
	}

	public boolean isGuest() {
		return guest;
	}

	public void setGuest(boolean guest) {
		this.guest = guest;
	}

}
