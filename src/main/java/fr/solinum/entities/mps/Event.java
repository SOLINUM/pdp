package fr.solinum.entities.mps;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * Created by Oan on 26/01/2018.
 */
@Entity
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private String start;
    private String end;
    private String backgroundColor;
    private String borderColor;
    private boolean editable;
    private String lieu;
    private String etatEnvoi;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModif;

    @ManyToOne
    private User user;
    
    @ManyToOne
    private Applicant applicant;

    public Event() {
    }

    public Event(String title, String description, String  start, String  end, User user, String backgroundColor, String borderColor, boolean editable) {
        this.title = title;
        this.description = description;
        this.start = start;
        this.end = end;
        this.user = user;
        this.backgroundColor = backgroundColor;
        this.borderColor = borderColor;
        this.editable = editable;
    }   
    

    public Event(String title, String description, String start, String end, boolean editable, String lieu,
			String etatEnvoi, User user, Applicant applicant, Date dateModif) {
		super();
		this.title = title;
		this.description = description;
		this.start = start;
		this.end = end;
		this.editable = editable;
		this.lieu = lieu;
		this.etatEnvoi = etatEnvoi;
		this.user = user;
		this.applicant = applicant;
		this.dateModif = dateModif;
	}

	public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public String getEtatEnvoi() {
		return etatEnvoi;
	}

	public void setEtatEnvoi(String etatEnvoi) {
		this.etatEnvoi = etatEnvoi;
	}

	public Date getDateModif() {
		return dateModif;
	}

	public void setDateModif(Date dateModif) {
		this.dateModif = dateModif;
	}	       
}
