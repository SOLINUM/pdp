package fr.solinum.entities.mps;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.solinum.mps.enumerate.DomaineApplication;
import fr.solinum.mps.enumerate.DureeBesoin;
import fr.solinum.mps.enumerate.EtatBesoin;
import fr.solinum.mps.enumerate.Provenance;
import fr.solinum.mps.enumerate.SecteurActiviteEntreprise;
import fr.solinum.mps.enumerate.TypeBesoin;

@Entity
@Table(name = "besoin")
public class Besoin {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "type_besoin")
	private TypeBesoin typeBesoin;

	@Column(name = "titre")
	private String titre;

	@Enumerated(EnumType.STRING)
	@Column(name = "etat_besoin")
	private EtatBesoin etatBesoin;

	@Column(name = "secteur")
	private String secteur;

	@Column(name = "lieu")
	private String lieu;

	@Enumerated(EnumType.STRING)
	@Column(name = "duree_besoin")
	private DureeBesoin dureeBesoin;

	@Enumerated(EnumType.STRING)
	@Column(name = "domaine_application")
	private DomaineApplication domaineApplication;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "domaine_intervention")
	private SecteurActiviteEntreprise domaineIntervention;

	@Column(name = "reference")
	private String reference;

	@Enumerated(EnumType.STRING)
	@Column(name = "provenance_besoin")
	private Provenance provenanceBesoin;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_demarrage")
	private Date dateDemarrage;

	@Column(name = "demarrage_immediate")
	private Boolean demarrageImmediate;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_reponse")
	private Date dateReponse;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_cloture")
	private Date dateCloture;

	@Column(name = "budget")
	private String budget;
	
	@Column(name = "commentaire")
	private String commentaire;

	@Column(name = "critere_requise", columnDefinition = "MEDIUMTEXT")
	private String critereRequise;

	@Column(name = "description", columnDefinition = "MEDIUMTEXT")
	private String description;

	@CreationTimestamp
	@Column(name = "date_creation")
	private Date dateCreation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modification")
	@UpdateTimestamp
	private Date dateModification;
	
	@JsonIgnore
	@OneToMany(mappedBy = "besoin", cascade = CascadeType.ALL)
	private List<Positionnement> positionnements;
	
	@ManyToOne
	private Contact contact;
	
	@Transient
	private String contactName;

	@ManyToOne
	private User responsable;
	
	@OneToOne
	@JoinColumn(name = "job_id")
	private Job job;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TypeBesoin getTypeBesoin() {
		return typeBesoin;
	}

	public void setTypeBesoin(TypeBesoin typeBesoin) {
		this.typeBesoin = typeBesoin;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public EtatBesoin getEtatBesoin() {
		return etatBesoin;
	}

	public void setEtatBesoin(EtatBesoin etatBesoin) {
		this.etatBesoin = etatBesoin;
	}

	public String getSecteur() {
		return secteur;
	}

	public void setSecteur(String secteur) {
		this.secteur = secteur;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public DureeBesoin getDureeBesoin() {
		return dureeBesoin;
	}

	public void setDureeBesoin(DureeBesoin dureeBesoin) {
		this.dureeBesoin = dureeBesoin;
	}

	public DomaineApplication getDomaineApplication() {
		return domaineApplication;
	}

	public void setDomaineApplication(DomaineApplication domaineApplication) {
		this.domaineApplication = domaineApplication;
	}
	
	public SecteurActiviteEntreprise getDomaineIntervention() {
		return domaineIntervention;
	}

	public void setDomaineIntervention(SecteurActiviteEntreprise domaineIntervention) {
		this.domaineIntervention = domaineIntervention;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Provenance getProvenanceBesoin() {
		return provenanceBesoin;
	}

	public void setProvenanceBesoin(Provenance provenanceBesoin) {
		this.provenanceBesoin = provenanceBesoin;
	}

	public Date getDateDemarrage() {
		return dateDemarrage;
	}

	public void setDateDemarrage(Date dateDemarrage) {
		this.dateDemarrage = dateDemarrage;
	}

	public Boolean getDemarrageImmediate() {
		return demarrageImmediate;
	}

	public void setDemarrageImmediate(Boolean demarrageImmediate) {
		this.demarrageImmediate = demarrageImmediate;
	}

	public Date getDateReponse() {
		return dateReponse;
	}

	public void setDateReponse(Date dateReponse) {
		this.dateReponse = dateReponse;
	}
	
	public Date getDateCloture() {
		return dateCloture;
	}

	public void setDateCloture(Date dateCloture) {
		this.dateCloture = dateCloture;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public String getCritereRequise() {
		return critereRequise;
	}

	public void setCritereRequise(String critereRequise) {
		this.critereRequise = critereRequise;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public List<Positionnement> getPositionnements() {
		return positionnements;
	}

	public void setPositionnements(List<Positionnement> positionnements) {
		this.positionnements = positionnements;
	}

	public User getResponsable() {
		return responsable;
	}

	public void setResponsable(User responsable) {
		this.responsable = responsable;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

}
