package fr.solinum.entities.mps;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.UpdateTimestamp;

import fr.solinum.mps.enumerate.StatusCRA;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "user_id", "date" }))
public class Cra implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@CreationTimestamp
	@Column(name = "date_creation")
	private Date dateCreation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modification")
	@UpdateTimestamp
	private Date dateModification;

	@Column(name = "commentaire")
	private String commentaire;

	@Column(name = "date")
	@Temporal(TemporalType.DATE)
	private Date date;

	@ManyToOne
	private User user;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private StatusCRA status;

	@OneToMany(mappedBy = "cra", cascade = CascadeType.REMOVE, fetch= FetchType.EAGER)
	@NotFound(action = NotFoundAction.IGNORE)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<DetailCra> detailsCra;

	@Column(name = "total_absence", columnDefinition = "double default 0")
	private double totalAbsence;

	@Column(name = "total_interne", columnDefinition = "double default 0")
	private double totalInterne;

	@Column(name = "total_production", columnDefinition = "double default 0")
	private double totalProduction;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public StatusCRA getStatus() {
		return status;
	}

	public void setStatus(StatusCRA status) {
		this.status = status;
	}

	public List<DetailCra> getDetailsCra() {
		return detailsCra;
	}

	public void setDetailsCra(List<DetailCra> detailsCra) {
		this.detailsCra = detailsCra;
	}

	public double getTotalAbsence() {
		return totalAbsence;
	}

	public void setTotalAbsence(double totalAbsence) {
		this.totalAbsence = totalAbsence;
	}

	public double getTotalInterne() {
		return totalInterne;
	}

	public void setTotalInterne(double totalInterne) {
		this.totalInterne = totalInterne;
	}

	public double getTotalProduction() {
		return totalProduction;
	}

	public void setTotalProduction(double totalProduction) {
		this.totalProduction = totalProduction;
	}

	@Override
	public String toString() {
		return "Cra [" + (id != null ? "id=" + id + ", " : "")
				+ (commentaire != null ? "commentaire=" + commentaire + ", " : "")
				+ (date != null ? "date=" + date + ", " : "") + (user != null ? "user=" + user + ", " : "")
				+ (status != null ? "status=" + status + ", " : "") + "totalAbsence=" + totalAbsence + ", totalInterne="
				+ totalInterne + ", totalProduction=" + totalProduction + "]";
	}

}
