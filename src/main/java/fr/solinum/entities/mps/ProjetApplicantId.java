package fr.solinum.entities.mps;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ProjetApplicantId implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "projet_id")
	private Long projetId;
	
	@Column(name = "applicant_id")
	private Long applicantId;
	
	public ProjetApplicantId() {
		super();
	}

	public ProjetApplicantId(Long projetId, Long applicantId) {
		super();
		this.projetId = projetId;
		this.applicantId = applicantId;
	}

	public Long getProjetId() {
		return projetId;
	}

	public void setProjetId(Long projetId) {
		this.projetId = projetId;
	}

	public Long getApplicantId() {
		return applicantId;
	}

	public void setApplicantId(Long applicantId) {
		this.applicantId = applicantId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((applicantId == null) ? 0 : applicantId.hashCode());
		result = prime * result + ((projetId == null) ? 0 : projetId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjetApplicantId other = (ProjetApplicantId) obj;
		if (applicantId == null) {
			if (other.applicantId != null)
				return false;
		} else if (!applicantId.equals(other.applicantId))
			return false;
		if (projetId == null) {
			if (other.projetId != null)
				return false;
		} else if (!projetId.equals(other.projetId))
			return false;
		return true;
	}


	
}
