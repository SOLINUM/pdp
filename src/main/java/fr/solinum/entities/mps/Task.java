package fr.solinum.entities.mps;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.DiffBuilder;
import org.apache.commons.lang3.builder.DiffResult;
import org.apache.commons.lang3.builder.Diffable;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by Oan on 18/01/2018.
 */

@Entity
public class Task implements Diffable<Task> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(columnDefinition = "MEDIUMTEXT")
	private String description;

	private String type;

	@ManyToOne
	private User user;

	@NotNull
	private Date targetDate;

	private boolean completed;

	private boolean approved;

	@Column(columnDefinition = "MEDIUMTEXT")
	private String compteRendu;

	@ManyToOne
	private User creator;

	@ManyToOne
	private Applicant applicant;
	
	@ManyToOne
	private Contact contact;
	
	@ManyToOne
	private InterlocuteurContact interlocuteurContact;

	public Task() {
	}

	public Task(User user, String description, Date targetDate, boolean completed) {
		this.user = user;
		this.targetDate = targetDate;
		this.description = description;
		this.completed = completed;
	}

	public Task(User user, String description, Date targetDate, boolean completed, User creator, boolean approved) {
		this.user = user;
		this.targetDate = targetDate;
		this.description = description;
		this.completed = completed;
		this.creator = creator;
		this.approved = approved;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(Date targetDate) {
		this.targetDate = targetDate;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCompteRendu() {
		return compteRendu;
	}

	public void setCompteRendu(String compteRendu) {
		this.compteRendu = compteRendu;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public InterlocuteurContact getInterlocuteurContact() {
		return interlocuteurContact;
	}

	public void setInterlocuteurContact(InterlocuteurContact interlocuteurContact) {
		this.interlocuteurContact = interlocuteurContact;
	}

	@Override
	public DiffResult<Task> diff(Task task) {
		DiffBuilder<Task> diffBuilder =  new DiffBuilder<Task>(this, task, ToStringStyle.SHORT_PREFIX_STYLE)
		           .append("Description", this.description, task.description)
		           .append("Type", this.type, task.type)
		           .append("Date cible", this.targetDate, task.targetDate)
		           .append("Fini", this.completed?"Oui":"Non", task.completed?"Oui":"Non")
		           .append("Apprové", this.approved?"Oui":"Non", task.approved?"Oui":"Non")
		           .append("Compte rendu", this.compteRendu, task.compteRendu);
		
//		if (this.user!=null && task.user!=null){
//			diffBuilder .append("responsable", this.user.getFirstName()+" "+ this.user.getLastName(),  task.user.getFirstName()+" "+ task.user.getLastName());
//		}
//		if (this.creator!=null && task.creator!=null){
//			diffBuilder.append("Auteur", this.creator.getFirstName()+" "+ this.creator.getLastName(), task.creator.getFirstName()+" "+ task.creator.getLastName());
//		}
		if (this.applicant!=null && task.applicant!=null){
			diffBuilder.append("Candidat", this.applicant.getName(), task.applicant.getName());
		}
		return diffBuilder.build();
	}

}
