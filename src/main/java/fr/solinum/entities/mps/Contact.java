package fr.solinum.entities.mps;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.solinum.mps.enumerate.EtatContact;
import fr.solinum.mps.enumerate.Provenance;
import fr.solinum.mps.enumerate.SecteurActiviteEntreprise;
import fr.solinum.mps.enumerate.StatusJuridique;

/**
 * Created by Adel on 18/01/2018.
 */

@Entity
public class Contact {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private User user;

	@Size(message = "Nom ne peut pas dépasser 20 caractères", max = 20, min = 0)
	private String name;

	@Size(message = "Prénom ne peut pas dépasser 20 caractères", max = 20, min = 0)
	private String firstName;

	@Size(message = "Nom contact ne peut pas dépasser 20 caractères", max = 20, min = 0)
	private String lastName;

	@Size(message = "Numéro Mobile ne peut pas dépasser 25 caractères", max = 25, min = 0)
	private String mobileNumber;

	@Size(message = "Numéro Fixe ne peut pas dépasser 25 caractères", max = 25, min = 0)
	private String phoneFixNumber;

	@Size(message = "Numéro Fax ne peut pas dépasser 25 caractères", max = 25, min = 0)
	private String fax;

	@Size(message = "Email ne peut pas dépasser 50 caractères", max = 50, min = 0)
	private String email;

	@Column(columnDefinition = "MEDIUMTEXT")
	private String notes;

	@Size(message = "Adresse ne peut pas dépasser 100 caractères", max = 100, min = 0)
	private String address;

	@Size(message = "Code postale ne peut pas dépasser 5 caractères", max = 5, min = 0)
	private String codePostale;

	@Size(message = "Ville ne peut pas dépasser 30 caractères", max = 30, min = 0)
	private String ville;

	@Size(message = "Site web ne peut pas dépasser 50 caractères", max = 50, min = 0)
	private String siteWeb;

	@Size(message = "Type Contact ne peut pas dépasser 11 caractères", max = 11, min = 0)
	private String typeContact;

	@Size(message = "Effectif ne peut pas dépasser 7 caractères", max = 7, min = 0)
	private String effectifsSociete;

	@Enumerated(EnumType.STRING)
	@Column(name = "etatContact")
	private EtatContact etatContact;

	@Enumerated(EnumType.STRING)
	@Column(name = "provenanceSociete")
	private Provenance provenanceSociete;

	@Size(message = "numeroTva ne peut pas dépasser 20 caractères", max = 20, min = 0)
	private String numeroTva;

	@Size(message = "siret ne peut pas dépasser 15 caractères", max = 15, min = 0)
	private String siret;

	@Enumerated(EnumType.STRING)
	@Column(name = "statusJuridique")
	private StatusJuridique statusJuridique;

	@Size(message = "rcs ne peut pas dépasser 30 caractères", max = 30, min = 0)
	private String rcs;

	@Size(message = "codeAPE ne peut pas dépasser 5 caractères", max = 5, min = 0)
	private String codeAPE;

	@Enumerated(EnumType.STRING)
	private SecteurActiviteEntreprise secteur;

	@Size(message = "numeroFounisseur ne peut pas dépasser 25 caractères", max = 25, min = 0)
	private String numeroFounisseur;

	@JsonIgnore
	@OneToMany(mappedBy = "contact")
	private List<InterlocuteurContact> interlocuteurContacts;

	@CreationTimestamp
	@Column(name = "date_creation")
	private Date dateCreation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modification")
	@UpdateTimestamp
	private Date dateModification;
	
	@OneToOne(cascade=CascadeType.ALL)
	private CoordonnesFacturation coordonnesFacturation;
	
	@OneToMany(mappedBy = "contact")
	@OrderBy("targetDate DESC")
	private List<Task> tasks;

	public Contact() {
	}

	public Contact(User user, String firstName, String lastName, String email, String mobileNumber, String notes,
			String address, String typeContact) {
		this.user = user;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.notes = notes;
		this.address = address;
		this.typeContact = typeContact;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPhoneFixNumber() {
		return phoneFixNumber;
	}

	public void setPhoneFixNumber(String phoneFixNumber) {
		this.phoneFixNumber = phoneFixNumber;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCodePostale() {
		return codePostale;
	}

	public void setCodePostale(String codePostale) {
		this.codePostale = codePostale;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getSiteWeb() {
		return siteWeb;
	}

	public void setSiteWeb(String siteWeb) {
		this.siteWeb = siteWeb;
	}

	public String getTypeContact() {
		return typeContact;
	}

	public void setTypeContact(String typeContact) {
		this.typeContact = typeContact;
	}

	public String getEffectifsSociete() {
		return effectifsSociete;
	}

	public void setEffectifsSociete(String effectifsSociete) {
		this.effectifsSociete = effectifsSociete;
	}

	public EtatContact getEtatContact() {
		return etatContact;
	}

	public void setEtatContact(EtatContact etatContact) {
		this.etatContact = etatContact;
	}

	public Provenance getProvenanceSociete() {
		return provenanceSociete;
	}

	public void setProvenanceSociete(Provenance provenanceSociete) {
		this.provenanceSociete = provenanceSociete;
	}

	public String getNumeroTva() {
		return numeroTva;
	}

	public void setNumeroTva(String numeroTva) {
		this.numeroTva = numeroTva;
	}

	public String getSiret() {
		return siret;
	}

	public void setSiret(String siret) {
		this.siret = siret;
	}

	public StatusJuridique getStatusJuridique() {
		return statusJuridique;
	}

	public void setStatusJuridique(StatusJuridique statusJuridique) {
		this.statusJuridique = statusJuridique;
	}

	public String getRcs() {
		return rcs;
	}

	public void setRcs(String rcs) {
		this.rcs = rcs;
	}

	public String getCodeAPE() {
		return codeAPE;
	}

	public void setCodeAPE(String codeAPE) {
		this.codeAPE = codeAPE;
	}

	public SecteurActiviteEntreprise getSecteur() {
		return secteur;
	}

	public void setSecteur(SecteurActiviteEntreprise secteur) {
		this.secteur = secteur;
	}

	public String getNumeroFounisseur() {
		return numeroFounisseur;
	}

	public void setNumeroFounisseur(String numeroFounisseur) {
		this.numeroFounisseur = numeroFounisseur;
	}

	public List<InterlocuteurContact> getInterlocuteurContacts() {
		return interlocuteurContacts;
	}

	public void setInterlocuteurContacts(List<InterlocuteurContact> interlocuteurContacts) {
		this.interlocuteurContacts = interlocuteurContacts;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public CoordonnesFacturation getCoordonnesFacturation() {
		if (coordonnesFacturation==null){
			coordonnesFacturation = new CoordonnesFacturation();
		}
		return coordonnesFacturation;
	}

	public void setCoordonnesFacturation(CoordonnesFacturation coordonnesFacturation) {
		this.coordonnesFacturation = coordonnesFacturation;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", user=" + user + ", name=" + name + ", firstName=" + firstName + ", lastName="
				+ lastName + ", mobileNumber=" + mobileNumber + ", phoneFixNumber=" + phoneFixNumber + ", fax=" + fax
				+ ", email=" + email + ", notes=" + notes + ", address=" + address + ", codePostale=" + codePostale
				+ ", ville=" + ville + ", siteWeb=" + siteWeb + ", typeContact=" + typeContact + ", effectifsSociete="
				+ effectifsSociete + ", etatContact=" + etatContact + ", provenanceSociete=" + provenanceSociete
				+ ", numeroTva=" + numeroTva + ", siret=" + siret + ", statusJuridique=" + statusJuridique + ", rcs="
				+ rcs + ", codeAPE=" + codeAPE + ", secteur=" + secteur + ", numeroFounisseur=" + numeroFounisseur
				+ ", interlocuteurContacts=" + interlocuteurContacts + ", dateCreation=" + dateCreation
				+ ", dateModification=" + dateModification + "]";
	}

}
