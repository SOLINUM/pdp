package fr.solinum.entities.mps;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "notes_positionnement")
public class NotePositionnement {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(columnDefinition = "MEDIUMTEXT")
	private String note;
	
	private String creepar;
	
	@CreationTimestamp
	@Column(name = "date_creation")
	private Date dateCreation;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modification")
	@UpdateTimestamp
	private Date dateModification;
	
	@ManyToOne
	private Positionnement positionnement;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Positionnement getPositionnement() {
		return positionnement;
	}

	public void setPositionnement(Positionnement positionnement) {
		this.positionnement = positionnement;
	}

	public String getCreepar() {
		return creepar;
	}

	public void setCreepar(String creepar) {
		this.creepar = creepar;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}
	
}
