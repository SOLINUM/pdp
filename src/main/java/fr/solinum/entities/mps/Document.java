package fr.solinum.entities.mps;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "document")
public class Document implements Serializable{ 

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum TypeDoc {
		URL, FICHIER;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Size(message = "Nom ne peut pas dépasser 100 caractères", max = 100)
	@NotEmpty(message = "Nom ne peut pas être vide")
	private String name;

	@Enumerated(EnumType.STRING)
	@Column(name = "type_doc")
	private TypeDoc typeDoc;

	@CreationTimestamp
	@Column(name = "date_creation")
	private Date dateCreation;

	@Column(name = "contenu")
	private String contenu;

	@Column(name = "note")
	private String note;

	@Column(name = "etat_envoi", columnDefinition="VARCHAR(45) default 'NOUVEAU'")
	private String etatEnvoi;

	@ManyToOne
	private Applicant applicant;

	@ManyToOne
	private Job job;

	@ManyToOne
	private User user;

	public Document() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Document(String name, TypeDoc typeDoc, Date dateCreation, String contenu, String note, Applicant applicant,
			Job job, User user) {
		super();
		this.name = name;
		this.typeDoc = typeDoc;
		this.dateCreation = dateCreation;
		this.contenu = contenu;
		this.note = note;
		this.applicant = applicant;
		this.job = job;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TypeDoc getTypeDoc() {
		return typeDoc;
	}

	public void setTypeDoc(TypeDoc typeDoc) {
		this.typeDoc = typeDoc;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getEtatEnvoi() {
		return etatEnvoi;
	}

	public void setEtatEnvoi(String etatEnvoi) {
		this.etatEnvoi = etatEnvoi;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
