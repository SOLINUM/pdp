package fr.solinum.entities.mps;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class DetailCra implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id; 
	
	@ManyToOne
	private Projet projet;
	
	@Column(name = "type_activite")
	private String typeActivite;
	
	@Column(name = "data")
	private String data;
	
	@Column(name = "valeur")
	private String valeur;
	
	@Column(name = "total")
	private Double total;

	@ManyToOne
	private Cra cra;
	
	@Column(name = "date_modification")
	@UpdateTimestamp
	private Date dateModification;
	
	@Transient
	Map<String, String> mapForData;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public String getTypeActivite() {
		return typeActivite;
	}

	public void setTypeActivite(String typeActivite) {
		this.typeActivite = typeActivite;
	}

	

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getValeur() {
		return valeur;
	}

	public void setValeur(String valeur) {
		this.valeur = valeur;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Cra getCra() {
		return cra;
	}

	public void setCra(Cra cra) {
		this.cra = cra;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public Projet getProjet() {
		return projet;
	}

	public void setProjet(Projet projet) {
		this.projet = projet;
	}

	public Map<String, String> getMapForData() {
		String tabs[] = data.split(",");
		mapForData = new HashMap<>();
		for (String std : tabs) {
			String str[] = std.split(":");
			mapForData.put(str[0], str.length>1?str[1]:"");
		}
		return mapForData;
	}

	public void setMapForData(Map<String, String> mapForData) {
		this.mapForData = mapForData;
	}
	
	public String returnValueOfMonth(String month) {	
		return getMapForData().get(month);
	}
}
