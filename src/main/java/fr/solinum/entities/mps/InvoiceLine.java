package fr.solinum.entities.mps;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fr.solinum.mps.enumerate.TvaEnum;
import fr.solinum.mps.enumerate.TypeLigneFacture;

@Entity
@Table(name = "invoiceLine")
public class InvoiceLine {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "type_ligne_facture")
	private TypeLigneFacture typeLigneFacture;  
	
	private String description;
	
	private Float tarifHt;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tva")
	private TvaEnum tvaEnum;
	
	private Float qte;
	
	private Float montantHt;
	
	private Float montantTtc;
	
	@ManyToOne
	private Invoice invoice;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TypeLigneFacture getTypeLigneFacture() {
		return typeLigneFacture;
	}

	public void setTypeLigneFacture(TypeLigneFacture typeLigneFacture) {
		this.typeLigneFacture = typeLigneFacture;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getTarifHt() {
		return tarifHt;
	}

	public void setTarifHt(Float tarifHt) {
		this.tarifHt = tarifHt;
	}

	public TvaEnum getTvaEnum() {
		return tvaEnum;
	}

	public void setTvaEnum(TvaEnum tvaEnum) {
		this.tvaEnum = tvaEnum;
	}

	public Float getMontantHt() {
		return montantHt;
	}

	public void setMontantHt(Float montantHt) {
		this.montantHt = montantHt;
		if (montantHt!=null && getInvoice()!=null && getInvoice().getTva()!=null){
			this.montantTtc =  this.montantHt * (1+  getInvoice().getTva());
		}
	}

	public Float getMontantTtc() {
		return montantTtc;
	}

	public void setMontantTtc(Float montantTtc) {
		this.montantTtc = montantTtc;
	}

	public Float getQte() {
		return qte;
	}

	public void setQte(Float qte) {
		this.qte = qte;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
	
	
}
