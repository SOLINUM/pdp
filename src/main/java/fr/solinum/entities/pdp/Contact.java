package fr.solinum.entities.pdp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Contact {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String lastname;
	private String telephonefixe;
	private String telephoneportable;
	private String emailpro;
	private String emailperso;
	@ManyToOne
	@JoinColumn(name = "partenaire_id")
	private Partenaire partenaire;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getTelephonefixe() {
		return telephonefixe;
	}

	public void setTelephonefixe(String telephonefixe) {
		this.telephonefixe = telephonefixe;
	}

	public String getTelephoneportable() {
		return telephoneportable;
	}

	public void setTelephoneportable(String telephoneportable) {
		this.telephoneportable = telephoneportable;
	}

	public String getEmailpro() {
		return emailpro;
	}

	public void setEmailpro(String emailpro) {
		this.emailpro = emailpro;
	}

	public String getEmailperso() {
		return emailperso;
	}

	public void setEmailperso(String emailperso) {
		this.emailperso = emailperso;
	}

	public Partenaire getPartenaire() {
		return partenaire;
	}

	public void setPartenaire(Partenaire partenaire) {
		this.partenaire = partenaire;
	}
	
}
