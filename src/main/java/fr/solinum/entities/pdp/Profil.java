package fr.solinum.entities.pdp;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import fr.solinum.model.pdp.Disponibilite;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Profil implements Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private String poste;
    private String prenom;
    private String tjm;
    private String comptences;
    private Integer nbr_exp;
    private Date creationDate;
    //private Blob cv;
    @Enumerated(EnumType.STRING)
    private Disponibilite disponible;
    @Lob
    private byte[]  cv;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPoste() {
		return poste;
	}
	public void setPoste(String poste) {
		this.poste = poste;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getTjm() {
		return tjm;
	}
	public void setTjm(String tjm) {
		this.tjm = tjm;
	}
	public Integer getNbr_exp() {
		return nbr_exp;
	}
	public void setNbr_exp(Integer nbr_exp) {
		this.nbr_exp = nbr_exp;
	}
	public Disponibilite getDisponible() {
		return disponible;
	}
	public void setDisponible(Disponibilite disponible) {
		this.disponible = disponible;
	}
	public byte[] getCv() {
		return cv;
	}
	public void setCv(byte[] cv) {
		this.cv = cv;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getComptences() {
		return comptences;
	}
	public void setComptences(String comptences) {
		this.comptences = comptences;
	}

}
