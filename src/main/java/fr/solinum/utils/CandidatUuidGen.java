package fr.solinum.utils;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CandidatUuidGen {

	Logger logger = LoggerFactory.getLogger(getClass());
	
	public String generateUid(){
		// Creating a random UUID (Universally unique identifier).
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        logger.debug("Random UUID String = " + randomUUIDString);
        logger.debug("UUID version       = " + uuid.version());
        logger.debug("UUID variant       = " + uuid.variant());
        return randomUUIDString;
	}
}
