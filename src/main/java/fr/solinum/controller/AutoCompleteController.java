package fr.solinum.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.solinum.entities.pdp.Contact;
import fr.solinum.service.ContactService;

@RestController
@RequestMapping("autocomplete")
public class AutoCompleteController {
	
	@Autowired
	private ContactService contactService;

	@GetMapping("/contact")
	public List<Contact> getContacts(@RequestParam(value = "q", required = false) String query) {
		
		if (StringUtils.isEmpty(query)) {
			return contactService.getAllContact();
		}else{
			return contactService.getContactByEmail(query);
		}
	}
}
