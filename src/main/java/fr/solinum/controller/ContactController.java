package fr.solinum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import fr.solinum.entities.pdp.Contact;
import fr.solinum.entities.pdp.Partenaire;
import fr.solinum.service.ContactService;
import fr.solinum.service.PartenaireService;

@Controller
public class ContactController {

	@Autowired
	private ContactService contactService;

	@Autowired
	private PartenaireService partenaireService;

	@GetMapping("/contact/{partenaireId}/new")
	public String addContactToPartenaire(Model model, @PathVariable Long partenaireId) {
		Partenaire partenaire = partenaireService.findById(partenaireId);
		model.addAttribute("partenaire", partenaire);
		model.addAttribute("contact", new Contact());
		return "contact/new";
	}

	@PostMapping("/contact/{partenaireId}/new")
	public String addContactToPartenaire(Contact contact, @PathVariable Long partenaireId) {
		Partenaire partenaire = partenaireService.findById(partenaireId);
		contactService.saveContact(contact);
		partenaire.getContacts().add(contact);
		partenaireService.savePartenaire(partenaire);
		return "redirect:/contact/" + partenaireId + "/new";
	}
}
