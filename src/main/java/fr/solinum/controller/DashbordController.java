package fr.solinum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import fr.solinum.entities.pdp.User;
import fr.solinum.service.CompagneService;

@Controller
public class DashbordController {
	
	@Autowired
	private CompagneService compagneService;

	@GetMapping("/")
	public String homeInit(Model model, Authentication authentication) {
		User userDetails = (User) authentication.getPrincipal();
		model.addAttribute("name", userDetails.getFullname());
		model.addAttribute("compagnes", compagneService.findAll());
		return "welcome";
	}

}
