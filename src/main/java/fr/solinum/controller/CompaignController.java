package fr.solinum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import fr.solinum.entities.pdp.Compagne;
import fr.solinum.entities.pdp.User;
import fr.solinum.repository.pdp.CompagneRepository;
import fr.solinum.service.AudienceService;
import fr.solinum.service.CompagneService;

@Controller
public class CompaignController {

	@Autowired
	private CompagneRepository compagneRepository;
	
	@Autowired
	private AudienceService audienceService;
	
	@Autowired
	private CompagneService compaignService;
	
	@GetMapping("/compaign/details/{compaignId}")
	public String showDetails(Model model, @PathVariable Long compaignId) {
		Compagne compaign  = compaignService.findById(compaignId);
		model.addAttribute("compaign", compaign);
		return "compaign/details";
	}

	
	@GetMapping("/compaign/new")
	public String showNew(Model model) {
		model.addAttribute("compaign", new Compagne());
		model.addAttribute("audiences", audienceService.findAudience());
		return "compaign/new";
	}
	
	@PostMapping("/compaign/new")
	public String addCompaign(Compagne compaign, Authentication authentication) {
		User userDetails = (User) authentication.getPrincipal();
		compaign.setCreatedby(userDetails.getFullname());
		compagneRepository.save(compaign);
		return "redirect:/";
	}
	
	@PostMapping("/compaign/{compaignId}/send")
	public String sendCompaign(Model model, @PathVariable Long compaignId) {
		Compagne compaign  = compaignService.findById(compaignId);
		compaignService.send(compaign);
		return "redirect:/compaign/details/"+compaignId;
	}
}
