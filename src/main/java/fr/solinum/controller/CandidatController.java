package fr.solinum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import fr.solinum.model.pdp.CandidatForm;
import fr.solinum.model.pdp.Constantes;
import fr.solinum.repository.pdp.ParametrageRepository;
import fr.solinum.utils.CandidatUuidGen;

@Controller
public class CandidatController {

	@Autowired
	private ParametrageRepository parametrageRepository;
	@Autowired
	private CandidatUuidGen candidatUuidGen;
	
	@GetMapping("/candidat/search")
	public String showSearchPage(Model model) {
		model.addAttribute("contrats_type", parametrageRepository.findParametrageByCode(Constantes.CONTRAT_TYPE).get());	
		return "candidat/search";
	}
	
	@PostMapping("/candidat/search")
	public String search(Model model) {
		return "candidat/search";
	}
	
	
	@GetMapping("/candidat/new")
	public String newCandidat(Model model) {
		model.addAttribute("candidatForm", new CandidatForm());
		model.addAttribute("candidat_uid", candidatUuidGen.generateUid());
		model.addAttribute("etapes_embauche", parametrageRepository.findParametrageByCode(Constantes.ETAPES_EMBAUCHE).get());
		model.addAttribute("provenance_cv", parametrageRepository.findParametrageByCode(Constantes.PROVENANCE_CV).get());
		model.addAttribute("disponibilites", parametrageRepository.findParametrageByCode(Constantes.CANDIDAT_DISPO).get());
		model.addAttribute("evaluations_global", parametrageRepository.findParametrageByCode(Constantes.CANDIDAT_EVALUATION_G).get());
		model.addAttribute("lespayes", parametrageRepository.findParametrageByCode(Constantes.PAYS).get());
		
		return "candidat/new";
	}
}
