package fr.solinum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import fr.solinum.entities.pdp.Audience;
import fr.solinum.service.AudienceService;
import fr.solinum.service.PartenaireService;

@Controller
public class AudienceController {

	@Autowired
	private AudienceService audienceService;
	
	@Autowired
	private PartenaireService partenaireService;
	
	@GetMapping("/audience")
	public String welcome(Model model) {
		model.addAttribute("audiences", audienceService.findAudience());
		return "audience/list";
	}
	
	@GetMapping("/audience/new")
	public String showNew(Model model) {
		model.addAttribute("audience", new Audience());
		model.addAttribute("allpartenaires", partenaireService.getAllPartenaire());
		return "audience/new";
	}
	
	@PostMapping("/audience/new")
	public String postNew(Audience audience) {
		audienceService.saveAudience(audience);
		return "redirect:/audience";
	}
	
	@GetMapping("/audience/{audienceId}")
	public String details(Model model, @PathVariable Long audienceId) {
		Audience audience =audienceService.findAudience(audienceId);
		model.addAttribute("audience", audience);
		return "audience/details";
	}


}
