package fr.solinum.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import fr.solinum.entities.pdp.Profil;
import fr.solinum.service.ProfilService;

@Controller
public class ProfilController {

	@Autowired
	private ProfilService profilService;

	Logger logger = LoggerFactory.getLogger(ProfilController.class);


	@Autowired
	private JmsTemplate jmsTemplate;
	
	@GetMapping()
	public String main(Model model) {
		model.addAttribute("profil", new Profil());
		return "add_candidate";
	}


	@PostMapping("/profil/add")
	public ResponseEntity<String> publish(@Valid  Profil profil  , BindingResult result, Model model) {
//		logger.info(message);
//		jmsTemplate.convertAndSend("test-queue", message);
		profilService.saveProfil(profil);
		return new ResponseEntity(HttpStatus.OK);
	}

	@PostMapping("/sendprofil")
	public ResponseEntity<String> publishliste(@RequestBody Profil profil) {
		String profiltext = profil.toString();

		jmsTemplate.convertAndSend("test-queue", profiltext);
		return new ResponseEntity(profil, HttpStatus.OK);
	}

	@PostMapping("/sendprofiltopic")
	public ResponseEntity<String> publishlistetopic(@RequestBody Profil profil) {
		String profiltext = profil.toString();

		jmsTemplate.convertAndSend("test-topic", profiltext);
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return new ResponseEntity(profil, HttpStatus.OK);
	}

	@PostMapping("/sendprofils")
	public ResponseEntity<String> publishliste(@RequestBody Profil[] profils) {

		for (Profil profil : profils) {

			String profiltext = profil.toString();

			jmsTemplate.convertAndSend("test-queue", profiltext);
		}

		return new ResponseEntity(profils, HttpStatus.OK);
	}

	@PostMapping("/sendprofilstopic")
	public ResponseEntity<String> publishlistetopic(@RequestBody Profil[] profils) {

		for (Profil profil : profils) {
			logger.info(profil.toString());
			String profiltext = profil.toString();

			jmsTemplate.convertAndSend("test-topic", profiltext);
		}

		return new ResponseEntity(profils, HttpStatus.OK);
	}
}
