package fr.solinum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import fr.solinum.entities.pdp.Partenaire;
import fr.solinum.service.PartenaireService;

@Controller
public class PartenaireController {

	@Autowired
	private PartenaireService partenaireService;
	
	@GetMapping("/partenaire")
	public String welcome(Model model) {
		model.addAttribute("partenaires", partenaireService.getAllPartenaire());
		return "partenaire/list";
	}
	
	@GetMapping("/partenaire/new")
	public String showNew(Model model) {
		model.addAttribute("partenaire", new Partenaire());
		return "partenaire/new";
	}
	
	@PostMapping("/partenaire/new")
	public String postNew(Partenaire partenaire) {
		partenaireService.savePartenaire(partenaire);
		return "redirect:/partenaire";
	}
	
}
