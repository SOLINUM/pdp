package fr.solinum.repository.pdp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.pdp.Profil;

@Repository
public interface ProfilRepository extends JpaRepository<Profil, Long> {
}
