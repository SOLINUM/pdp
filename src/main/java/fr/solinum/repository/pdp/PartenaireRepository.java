package fr.solinum.repository.pdp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.pdp.Partenaire;

@Repository
public interface PartenaireRepository  extends JpaRepository<Partenaire, Long>{

}
