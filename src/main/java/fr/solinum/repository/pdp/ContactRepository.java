package fr.solinum.repository.pdp;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.pdp.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

	@Query(" select c from Contact c  where c.emailpro = ?1 or c.emailperso= ?2 ")
	Optional<Contact> getContactByEmailProOrPerso(String emailPro, String emailPerso);
	
	@Query(value="select c from Contact c where c.emailpro like %:email% or c.emailperso like %:email%")
	List<Contact> getContactByEmailProOrPerso(@Param("email") String keyword);
}
