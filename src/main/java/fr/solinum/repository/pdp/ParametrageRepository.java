package fr.solinum.repository.pdp;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.pdp.Parametrage;

@Repository
public interface ParametrageRepository  extends JpaRepository<Parametrage, Long>{

	@Query(" select p from Parametrage p  where p.code = ?1")
	Optional<List<Parametrage>> findParametrageByCode(String code);
}
