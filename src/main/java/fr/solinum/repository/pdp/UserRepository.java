package fr.solinum.repository.pdp;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.pdp.User;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	@Query(" select u from User u  where u.username = ?1")
	Optional<User> findUserWithName(String username);
}