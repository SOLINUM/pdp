package fr.solinum.repository.pdp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.pdp.Compagne;

@Repository
public interface CompagneRepository extends JpaRepository<Compagne, Long> {

}
