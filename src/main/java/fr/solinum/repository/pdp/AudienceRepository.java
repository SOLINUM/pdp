package fr.solinum.repository.pdp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.pdp.Audience;

@Repository
public interface AudienceRepository extends JpaRepository<Audience, Long> {

}
