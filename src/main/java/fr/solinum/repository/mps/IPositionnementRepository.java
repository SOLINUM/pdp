package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.mps.Positionnement;
@Repository
public interface IPositionnementRepository extends JpaRepository<Positionnement, Long>{

}
