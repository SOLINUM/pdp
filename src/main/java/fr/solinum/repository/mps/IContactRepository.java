package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.mps.Contact;

@Repository
public interface IContactRepository extends JpaRepository<Contact, Long>{

}
