package fr.solinum.repository.mps;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.mps.Applicant;
@Repository
public interface IApplicantRepository extends JpaRepository<Applicant,Long>{
	List <Applicant> findByNameContaining(String queryString);

}
