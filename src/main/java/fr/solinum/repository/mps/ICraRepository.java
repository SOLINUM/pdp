package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import fr.solinum.entities.mps.Cra;
@Repository
public interface ICraRepository extends JpaRepository<Cra, Long>{

}
