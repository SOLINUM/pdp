package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.mps.Event;
@Repository
public interface IEventRepository extends JpaRepository<Event, Long>{

}
