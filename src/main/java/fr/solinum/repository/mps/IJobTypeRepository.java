package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.mps.JobType;
@Repository
public interface IJobTypeRepository extends JpaRepository<JobType, Long>{

}
