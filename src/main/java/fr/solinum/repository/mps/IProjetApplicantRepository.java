package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import fr.solinum.entities.mps.ProjetApplicant;
import fr.solinum.entities.mps.ProjetApplicantId;
@Repository
public interface IProjetApplicantRepository extends JpaRepository<ProjetApplicant, ProjetApplicantId>{

}
