package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import fr.solinum.entities.mps.JobApplicant;
@Repository
public interface IJobApplicantRepository extends JpaRepository<JobApplicant, Long>{

}
