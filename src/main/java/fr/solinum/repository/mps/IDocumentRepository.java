package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.mps.Document;

@Repository
public interface IDocumentRepository extends JpaRepository<Document, Long>{

}
