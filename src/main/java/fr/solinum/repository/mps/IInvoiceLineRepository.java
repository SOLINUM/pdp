package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.mps.InvoiceLine;
@Repository
public interface IInvoiceLineRepository extends JpaRepository<InvoiceLine, Long>{

}
