package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import fr.solinum.entities.mps.Role;
@Repository
public interface IRoleRepository extends JpaRepository<Role, Long>{

}
