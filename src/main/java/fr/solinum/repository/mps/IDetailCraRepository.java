package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import fr.solinum.entities.mps.DetailCra;
@Repository
public interface IDetailCraRepository extends JpaRepository<DetailCra, Long>{

}
