package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import fr.solinum.entities.mps.Projet;
@Repository
public interface IProjetRepository extends JpaRepository<Projet, Long>{

}
