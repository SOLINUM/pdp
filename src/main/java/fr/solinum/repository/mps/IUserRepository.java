package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import fr.solinum.entities.mps.User;
@Repository
public interface IUserRepository extends JpaRepository<User, Long>{

}
