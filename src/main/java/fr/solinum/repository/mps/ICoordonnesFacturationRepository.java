package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import fr.solinum.entities.mps.CoordonnesFacturation;

@Repository
public interface ICoordonnesFacturationRepository extends JpaRepository<CoordonnesFacturation, Long>{

}
