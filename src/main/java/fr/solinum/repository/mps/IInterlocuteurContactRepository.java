package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import fr.solinum.entities.mps.InterlocuteurContact;
@Repository
public interface IInterlocuteurContactRepository extends JpaRepository<InterlocuteurContact, Long>{

}
