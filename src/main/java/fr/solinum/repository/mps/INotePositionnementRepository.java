package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import fr.solinum.entities.mps.NotePositionnement;
@Repository
public interface INotePositionnementRepository extends JpaRepository<NotePositionnement, Long>{

}
