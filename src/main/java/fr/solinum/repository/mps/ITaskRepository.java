package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import fr.solinum.entities.mps.Task;
@Repository
public interface ITaskRepository extends JpaRepository<Task, Long>{

}
