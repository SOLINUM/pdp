package fr.solinum.repository.mps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.solinum.entities.mps.Job;
@Repository
public interface IJobRepository extends JpaRepository<Job, Long>{

}
