package fr.solinum.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.solinum.entities.pdp.Profil;
import fr.solinum.repository.pdp.ProfilRepository;
import fr.solinum.service.ProfilService;

@Service
public class ProfilImpl implements ProfilService {
    @Autowired
    private ProfilRepository profilRepository;

    @Override
    public Profil saveProfil(Profil profil) {
        return profilRepository.save(profil);
    }

    @Override
    public List<Profil> getlistProfil() {
        return profilRepository.findAll();
    }
}