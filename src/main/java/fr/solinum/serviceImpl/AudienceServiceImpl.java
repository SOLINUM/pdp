package fr.solinum.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.solinum.entities.pdp.Audience;
import fr.solinum.repository.pdp.AudienceRepository;
import fr.solinum.service.AudienceService;

@Service
public class AudienceServiceImpl implements AudienceService {

	@Autowired
	private AudienceRepository audienceRepository;
	
	@Override
	public List<Audience> findAudience() {
		return audienceRepository.findAll();
	}

	@Override
	public void saveAudience(Audience audience) {
		audienceRepository.save(audience);
	}

	@Override
	public Audience findAudience(Long audienceId) {
		return audienceRepository.findById(audienceId).get();
	}

}
