package fr.solinum.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.solinum.entities.pdp.Contact;
import fr.solinum.repository.pdp.ContactRepository;
import fr.solinum.service.ContactService;

@Service
public class ContactServiceImpl implements ContactService{
	
	@Autowired
	private ContactRepository contactRepository;

	@Override
	public boolean saveContact(Contact contact) {
		contactRepository.saveAndFlush(contact);
		return true;
	}

	@Override
	public Contact getContactByEmailProOrPerso(String emailPro, String emailPerso) {
		return contactRepository.getContactByEmailProOrPerso(emailPro, emailPerso).orElse(null);
	}

	@Override
	public List<Contact> getContactByEmail(String email) {
		return contactRepository.getContactByEmailProOrPerso(email);
	}

	@Override
	public List<Contact> getAllContact() {
		return contactRepository.findAll();
	}

}
