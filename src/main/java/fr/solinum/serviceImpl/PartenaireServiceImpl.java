package fr.solinum.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.solinum.entities.pdp.Partenaire;
import fr.solinum.repository.pdp.PartenaireRepository;
import fr.solinum.service.PartenaireService;

@Service
public class PartenaireServiceImpl implements PartenaireService {

	@Autowired
	private PartenaireRepository partenaireRepository;
	
	
	@Override
	public List<Partenaire> getAllPartenaire() {
		return partenaireRepository.findAll();
	}


	@Override
	public void savePartenaire(Partenaire partenaire) {
		partenaireRepository.save(partenaire);
	}


	@Override
	public Partenaire findById(Long partenaireId) {
		return partenaireRepository.findById(partenaireId).orElse(null);
	}

}
