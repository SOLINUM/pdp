package fr.solinum.serviceImpl;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;

import fr.solinum.entities.pdp.Compagne;
import fr.solinum.repository.pdp.CompagneRepository;
import fr.solinum.service.CompagneService;

@Service
public class CompagneServiceImpl implements CompagneService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Value("${emailing.apikey.value}")
	private String apiKey;

	@Autowired
	private CompagneRepository compagneRepository;

	@Override
	public List<Compagne> findAll() {
		return compagneRepository.findAll();
	}

	@Override
	public Compagne findById(Long compaignId) {
		return compagneRepository.findById(compaignId).orElse(null);
	}

	@Override
	public void send(Compagne compaign) {
		Email from = new Email("abdessalem.belhadj@solinum.fr");
		String subject = compaign.getCompaignparam().getSubject();
		Email to = new Email(compaign.getAudience().getPartenaires().get(0).getContacts().get(0).getEmailpro());
		Content content = new Content("text/plain", compaign.getDescription());
		Mail mail = new Mail(from, subject, to, content);

		SendGrid sg = new SendGrid(apiKey);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
//			logger.info(response.getStatusCode());
//			System.out.println(response.getBody());
//			System.out.println(response.getHeaders());
		} catch (IOException ex) {
			logger.error(ex.getMessage());
		}
	}

}
