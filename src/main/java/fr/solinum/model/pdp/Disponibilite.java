package fr.solinum.model.pdp;

public enum Disponibilite {
  IMMEDIATE,UN_MOIS,DEUX_MOIS,PLUS_DEUX_MOIS
}
