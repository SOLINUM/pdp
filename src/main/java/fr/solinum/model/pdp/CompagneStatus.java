package fr.solinum.model.pdp;

public enum CompagneStatus {

	ACTIF, BROUILLON, TERMINEE, ENVOYEE;
}
