package fr.solinum.model.pdp;

import java.io.Serializable;

public class CandidatForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String uid;
	private String etape_embauche;
	private String provenance_cv;
	private String disponibilite;
	private String evaluation;
	private String pays;

	
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getEtape_embauche() {
		return etape_embauche;
	}

	public void setEtape_embauche(String etape_embauche) {
		this.etape_embauche = etape_embauche;
	}

	public String getProvenance_cv() {
		return provenance_cv;
	}

	public void setProvenance_cv(String provenance_cv) {
		this.provenance_cv = provenance_cv;
	}

	public String getDisponibilite() {
		return disponibilite;
	}

	public void setDisponibilite(String disponibilite) {
		this.disponibilite = disponibilite;
	}

	public String getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

}
