package fr.solinum.model.mps;

import java.io.Serializable;

public class DiffBetween2Objects implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String key;

	private Object oldValue;

	private Object newValue;
	

	public DiffBetween2Objects(String key, Object oldValue, Object newValue) {
		super();
		this.key = key;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getOldValue() {
		return oldValue;
	}

	public void setOldValue(Object oldValue) {
		this.oldValue = oldValue;
	}

	public Object getNewValue() {
		return newValue;
	}

	public void setNewValue(Object newValue) {
		this.newValue = newValue;
	}

}
