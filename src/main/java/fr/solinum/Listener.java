package fr.solinum;

import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import fr.solinum.entities.pdp.Profil;

@Component
public class Listener {
	public static final String ORDER_TOPIC = "test-topic";
	public static List<Profil> profils;
	private final Logger logger = LoggerFactory.getLogger(MessageConsumer.class);

	@JmsListener(destination = "test-queue")
	public void listener(String message) {
		logger.info("Message received {} ", message);
	}

	@JmsListener(destination = "test-topic")
	public void listenertopic(String message) throws JMSException {
		logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");

		logger.info("Message received {} ", message);

	}

	@JmsListener(destination = "inbound.topic")
	@SendTo("outbound.topic")
	public String receiveMessageFromTopic(final Message jsonMessage) throws JMSException {
		String messageData = null;
		System.err.println("Received message " + jsonMessage);
		String response = null;

		if (jsonMessage instanceof TextMessage) {
			TextMessage textMessage = (TextMessage) jsonMessage;
			messageData = textMessage.getText();
			Map map = new Gson().fromJson(messageData, Map.class);
			response = "Hello " + map.get("name");

		}

		return response;
	}

//	public void sendMessage(String fileName) {
//		jmsTemplate.send(new MessageCreator() {
//
//			@Override
//			public Message createMessage(Session session) throws JMSException {
//				try {
//					BytesMessage bytesMessage = session.createBytesMessage();
//					FileInputStream fileInputStream = new FileInputStream(fileName);
//					final int BUFLEN = 64;
//					byte[] buf1 = new byte[BUFLEN];
//					int bytes_read = 0;
//					while ((bytes_read = fileInputStream.read(buf1)) != -1) {
//						bytesMessage.writeBytes(buf1, 0, bytes_read);
//					}
//					fileInputStream.close();
//					return bytesMessage;
//				} catch (Exception e) {
//					return null;
//				}
//
//			}
//		});
//	}

	/*
	 * @JmsListener(destination = ORDER_QUEUE) public void receiveMessage(@Payload
	 * order,
	 * 
	 * @Headers MessageHeaders headers, Message message, Session session) {
	 * logger.info("received <" + order + ">");
	 * 
	 * logger.info("- - - - - - - - - - - - - - - - - - - - - - - -");
	 * logger.info("######          Message Details           #####");
	 * logger.info("- - - - - - - - - - - - - - - - - - - - - - - -");
	 * logger.info("headers: " + headers); logger.info("message: " + message);
	 * logger.info("session: " + session);
	 * logger.info("- - - - - - - - - - - - - - - - - - - - - - - -"); }
	 */
	// @JmsListener(destination = "inbound.queue")
//    @SendTo("outbound.queue")
//	public String receiveMessage(final Message jsonMessage) throws JMSException {
//		String messageData = null;
//		System.err.println("Received message " + jsonMessage);
//		String response = null;
//		if(jsonMessage instanceof TextMessage) {
//			TextMessage textMessage = (TextMessage)jsonMessage;
//			messageData = textMessage.getText();
//			Map map = new Gson().fromJson(messageData, Map.class);
//			response  = "Bonjour " + map.get("name");
//		}
//		return response;
//	}

}
