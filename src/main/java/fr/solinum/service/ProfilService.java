package fr.solinum.service;

import java.util.List;

import fr.solinum.entities.pdp.Profil;

public interface ProfilService {
    Profil saveProfil(Profil profil) ;
    List<Profil> getlistProfil() ;
}
