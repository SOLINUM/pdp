package fr.solinum.service;

import java.util.List;

import fr.solinum.entities.pdp.Contact;

public interface ContactService {

	/**
	 * permet d'ajouter un contact dans une base de donnée
	 * 
	 * @param contact
	 * @return
	 */
	boolean saveContact(Contact contact);

	/**
	 * obtenir le contact par son mail perso ou pro
	 * 
	 * @param email
	 * @return
	 */
	Contact getContactByEmailProOrPerso(String emailPro, String emailPerso);

	/***
	 * getContactByEmail
	 * @param email
	 * @return
	 */
	List<Contact> getContactByEmail(String email);
	
	/**
	 * obtenir tout les contacts
	 * @return
	 */
	List<Contact> getAllContact();
}
