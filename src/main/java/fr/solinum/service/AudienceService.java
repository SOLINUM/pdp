package fr.solinum.service;

import java.util.List;

import fr.solinum.entities.pdp.Audience;

public interface AudienceService {

	/**
	 * chercherc toute les audience
	 * @return
	 */
	List<Audience> findAudience();
	
	/**
	 * Sauvegarder une audience
	 * @param audience
	 */
	void saveAudience(Audience audience);
	
	/**
	 * rechercher une audince par son ID
	 * @param audienceId
	 * @return
	 */
	Audience findAudience(Long audienceId);
}
