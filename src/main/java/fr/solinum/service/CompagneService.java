package fr.solinum.service;

import java.util.List;

import fr.solinum.entities.pdp.Compagne;

public interface CompagneService {

	/**
	 * chercher toutes les compagness
	 * @return
	 */
	List<Compagne> findAll();
	
	/***
	 * obtenir une compagne par son Id
	 * @param compaignId
	 * @return
	 */
	Compagne findById(Long compaignId);

	/**
	 * envoyer un compagne d'email
	 * @param compaign
	 */
	void send(Compagne compaign);
}
