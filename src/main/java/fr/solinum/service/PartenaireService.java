package fr.solinum.service;

import java.util.List;

import fr.solinum.entities.pdp.Partenaire;


public interface PartenaireService {

	/**
	 * getAllPartenaire
	 * @return List<Partenaire>
	 */
	List<Partenaire> getAllPartenaire();

	/**
	 * Save partenaire into Database
	 * @param partenaire
	 */
	void savePartenaire(Partenaire partenaire);

	/**
	 * findById
	 * @param partenaireId
	 * @return Partenaire
	 */
	Partenaire findById(Long partenaireId);
}
