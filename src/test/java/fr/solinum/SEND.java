package fr.solinum;

import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.ClientOptions;
import com.mailjet.client.resource.Emailv31;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SEND {

	private static String MJ_APIKEY_PUBLIC = "06b71104b4eb0cc4961c8d5ea1e4fb7a";
	private static String MJ_APIKEY_PRIVATE = "605270720d85010c2d0477cb5cb919a4";
	private static String SENDER_EMAIL = "abdessalem.belhadj@solinum.fr";
	private static String RECIPIENT_EMAIL = "abdessalem.belhadj@solinum.fr";

	public static void main(String[] args) throws MailjetException, MailjetSocketTimeoutException, JSONException {
		MailjetClient client;
		MailjetRequest request;
		MailjetResponse response;
		client = new MailjetClient(MJ_APIKEY_PUBLIC, MJ_APIKEY_PRIVATE, new ClientOptions("v3.1"));
		client.setDebug(MailjetClient.VERBOSE_DEBUG);
		   request = new MailjetRequest(Emailv31.resource)
		            .property(Emailv31.MESSAGES, new JSONArray()
		                .put(new JSONObject()
		                    .put(Emailv31.Message.FROM, new JSONObject()
		                        .put("Email",SENDER_EMAIL)
		                        .put("Name", "Me"))
		                    .put(Emailv31.Message.TO, new JSONArray()
		                        .put(new JSONObject()
		                            .put("Email", RECIPIENT_EMAIL)
		                            .put("Name", "You")))
		                    .put(Emailv31.Message.SUBJECT, "My first Mailjet Email!")
		                    .put(Emailv31.Message.TEXTPART, "Greetings from Mailjet!")
		                    .put(Emailv31.Message.HTMLPART, "<h3>Dear passenger 1, welcome to <a href=\"https://www.mailjet.com/\">Mailjet</a>!</h3><br />May the delivery force be with you!")));
		      response = client.post(request);
		      System.out.println(response.getStatus());
		      System.out.println(response.getData());
	}

}
